
/**
 * Event class for implementing observer pattern
 * @param {Object} sender Target dispatcher for this event
 * @constructor
 */
com.rehabstudio.Event = function( sender )
{

	/**
	 * Target dispatcher for this event
	 * @private
	 * @type {Object}
	 */
	this._sender = sender;


	/**
	 * List of callbacks subscribed to this event
	 * @private
	 * @type Array.<Function>
	 */
	this._observers = [];
}

/**
 * Attaches an observer to this event
 * @param {Function} observer An observer to attach to this event
 */
com.rehabstudio.Event.prototype.attach = function( observer )
{
	this._observers.push( observer );
}

/**
 * Attaches an observer to this event
 * @param {Function} observer An observer to attach to this event
 */
com.rehabstudio.Event.prototype.detach = function( observer )
{

	var index = $.inArray(observer, this._observers);

	if(index<0)
	{
		return;
	}

	this._observers.splice(index,1);

}

/**
 * Attaches an observer to this event and pass in a bind for closure
 * @param {Function} observer An observer to attach to this event
 * @param {Object}	 ref	  The object to which the observer is attached
 */
com.rehabstudio.Event.prototype.attach_bind = function( observer,ref )
{
	this._observers.push( {'observer':observer,'ref':ref});
};

/**
 * Dispatches this event with given parameters to all observers
 * @param args zero or more arguments which will be passed to observers
 */
com.rehabstudio.Event.prototype.notify = function( args )
{
	for( var i = 0; i < this._observers.length; ++i )
	{
		var callFunc = this._observers[i];
		if(callFunc.observer !== undefined){
			callFunc.observer.apply(callFunc.ref,[this._sender,args]);
		} else {
			callFunc( this._sender, args );
		}
	}
};