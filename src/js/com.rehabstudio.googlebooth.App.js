var $j = jQuery.noConflict();

$j(document).ready(function()
{

	$j.Body = $j('body');
    $j.Window = $j(window);
    $j.Document = $j(document);

	$j.Document.bind("contextmenu", function(e) {
		return false;
	});

	var configUrl = "data/config.json";
	//$j.Body.addClass("staging");
    /*if(window.location.href.indexOf("photobooth")>=0){
    	configUrl = "data/config.staging.json";
    	$j.Body.addClass("staging");
	}

	if(window.location.href.indexOf("dev")>=0){
    	configUrl = "data/config.dev.json";
    	$j.Body.addClass("staging");
	}*/




	var	controller,
		service,
		model,
		mediator,
		view,
		dom = {};

		dom.body = $j.Body;

		service		= new com.rehabstudio.googlebooth.service.FSService();

		model		= new com.rehabstudio.googlebooth.Model(service, configUrl);
		controller	= new com.rehabstudio.googlebooth.Controller(model);
		view		= new com.rehabstudio.googlebooth.View(dom);
		mediator	= new com.rehabstudio.googlebooth.ViewMediator(model, view, controller );

		view.mediateView(mediator);
		mediator.execute();

		$j.Window.bind('resize', function(e)
		{
				view.resize($j.Window.width(), $j.Window.height());
		});


		view.resize($j.Window.width(), $j.Window.height());


		animate();
		function animate()
		{
			requestAnimationFrame( animate );
			TWEEN.update();
		}

		model.init();
});