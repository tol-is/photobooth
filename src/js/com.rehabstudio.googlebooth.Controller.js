
/**
 * Controller component of the Model View Controller implementation
 * @param {*} model The data Model for this Controller
 * @constructor
 */
com.rehabstudio.googlebooth.Controller = function( model ) {
	
	var self = this;
	
	//this._dbgTimer = null;
	
	this._connCheckTO = null;

	this._model = model;
	
	this._publishTimer = new com.rehabstudio.utils.Timer("publishTimer", 20);
	this._standByTimer = new com.rehabstudio.utils.Timer("standByTimer", 40);
	this._slideshowUpdateTimer = new com.rehabstudio.utils.Timer("slideshowUpdateTimer", 60*10);

	this._standByTimer.timerEvent.attach( function(sender, response)
	{
		if(response.source == "standByTimer")
			self._model.resetApp();

	});
	this._standByTimer.startTimer();


	this._publishTimer.timerEvent.attach( function(sender, args)
	{
		if(args.source == "publishTimer")
			self._model.publishData();
			
	});

	this._slideshowUpdateTimer.timerEvent.attach( function(sender, response)
	{
		if(response.source == "slideshowUpdateTimer")
			self._model.fetchNewSlideshowImages();
	});


	this._model.modelEvent.attach( function(sender, response)
	{
		switch(true)
		{
			case response.message === "fs_init":
				self._testConnection();
				Logger.debug('Controller : respond to fs_init - _testConnection() before srtart');
			break;

			case response.message === "batch_exit_restart":
				self._testConnection();
				Logger.debug('Controller : respond to batch_exit_restart - _testConnection() before start()');
			break;

			case response.message === "data_publish_start":
				self.cron_stop();
			break;

			default:
				Logger.debug(response.message);
		}
	});

}


com.rehabstudio.googlebooth.Controller.prototype.initRemoteImagesService = function()
{
	this._slideshowUpdateTimer.forceTick();
	this._slideshowUpdateTimer.startTimer();
}

com.rehabstudio.googlebooth.Controller.prototype.cron_start = function()
{
	var self = this;
	this._publishTimer.resetTimer();
	this._publishTimer.startTimer();

	Logger.debug ('cron started');
}

com.rehabstudio.googlebooth.Controller.prototype.cron_stop = function()
{
	this._publishTimer.stopTimer();
}


com.rehabstudio.googlebooth.Controller.prototype._testConnection = function()
{

				//this.cron_start();
				//return false;
				var guid = this._model.getAppGUID();
				var baseurl = this._model.getAppBaseURL();
				var self = this;
				var testData = {};//{test:'connnectivity_test'};
				Logger.debug('Connectivity test start');
				$j.ajax(
				{
                    url: baseurl + guid,
                    type: 'GET',
                    dataType:'json',
                    data:testData,
                    success: function(result)
                    {
						self._model.isAppOnline = true;
                        Logger.debug('Controller connectivity test succcessful, ok to go');
                        self.cron_start();
                        if(this._connCheckTO)clearTimeout(this._connCheckTO);
                    },
                    error : function()
                    {
						self._model.takeAppOffline();
						Logger.debug('Controller connectivity test failed, aborting Cron, Model should have been taken offline (check upload flags)');

						this._connCheckTO = setTimeout(function(){
							self._testConnection();
						},20*1000);
                    }
                });

}



com.rehabstudio.googlebooth.Controller.prototype.startStandByTimer = function()
{
	this._standByTimer.startTimer();
}

com.rehabstudio.googlebooth.Controller.prototype.resetStandByTimer = function()
{
	Logger.debug("reset stand by");
	var self = this;
	this._standByTimer.resetTimer();
}