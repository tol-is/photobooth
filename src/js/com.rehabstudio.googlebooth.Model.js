/**
 * Model component of the Model View Controller implementation
 * @param {com.rehabstudio.googlebooth.service.FSService} service External communication Object, File system operations wrapper (User and Log related operations)
 * @param {String} configuration json url
 * @constructor
 */

com.rehabstudio.googlebooth.Model = function( service, configUrl )
{
	var self = this;

	/**
    * Bool - Flag to indicate wether the app is connected or not. Mostly handled by the Controller
    * @type {Bool}
    */
	this.isAppOnline = false;

	/**
    * com.rehabstudio.Event - Event object to broadcast model states
    * @type {com.rehabstudio.Event}
    */
	this.modelEvent = new com.rehabstudio.Event( this );

    /**
    * com.rehabstudio.googlebooth.service.FSService - Used for all operations involving the file system
    * @type {com.rehabstudio.googlebooth.service.FSService}
    */
	this.fsService = service;

	/**
    * Object - Stores locally all configuration parameters, accessed by the getters to return configuration values
    * @type {Object}
    */
	this.eventConfig = {};

	/**
    * Object - Stores locally all app translations
    * @type {Object}
    */
	this.languageConfig = {};

	/**
    * String - Stores default lang string
    * @type {String}
    */
	this.currentLang = "";

	/**
    * Instance of the class which returns Configuration VOs to be stored in the eventConfig local property - Application settings
    * @type {com.rehabstudio.googlebooth.remote.ConfigService}
    */
	this.configService = new com.rehabstudio.googlebooth.remote.ConfigService(configUrl);

	/**
    * Instance of the class which returns Configuration VOs to be stored in the languageConfig local property
    * @type {com.rehabstudio.googlebooth.remote.ConfigService}
    */
	this.languageService = new com.rehabstudio.googlebooth.remote.ConfigService("data/lang.json");

	/**
    * String - String inserted in the email+token users identifier when concatenating them. The purpose is to be able to easily split the values at a later stage
    * @type {String}
    */
	this.tokenSeparator = '#';

	/**
    * Array - A copy of the application logs held locally in array format (i.e. each array item corresponds to a log entry)
    * @type {Array}
    */
	this.logs = [];

	/**
    * String - The application unique identifier within the backend
    * @type {String}
    */
	this.appGUID = this.appBaseURL = null;

	/**
    * Bool - Flag, indicates the Model has requested a logs write to the File System Service and is hanging idle in wait of results (and waiting to be passed fresh logs). When false, the batch process won't run
    * @type {Bool}
    */
	this.waitingForLogsRefresh = false;

	/**
    * Array - Where all Images VOs are saved as users take images in the Booth view. Cleared after submission or on new pics - i.e. never holdsd more than 4 images and only supports one user at a time
    * @type {Array}
    */
	this.userImages = [];

	/**
    * Integer - Count of images saved, used to double check on save process state
    * @type {Int}
    */
	this.imagesSaved = 0;

	/**
    * com.rehabstudio.googlebooth.remote.PicasaService - Instance of the remote service in charge of sending images via Ajax to the backend
    * @type {com.rehabstudio.googlebooth.remote.PicasaService}
    */
	this.remote = new com.rehabstudio.googlebooth.remote.PicasaService();

	/**
    * Bool - Flag, indicates a publish (cron) service is running and a new one should be rejected
    * @type {Bool}
    */
	this.picasaPublishRunning = false;

	/**
    * Bool - Flag, indicates a realtime publish service is running and a new one  should be rejected
    * @type {Bool}
    */
	this.picasaRealtimePublishRunning = false;

	/**
    * com.rehabstudio.googlebooth.remote.SlideshowService - Instance of the remote service in charge of fetching new images for the standby slideshow
    * @type {com.rehabstudio.googlebooth.remote.SlideshowService}
    */
	this.slideshowService = new com.rehabstudio.googlebooth.remote.SlideshowService();

	/**
    * Array - Where the preconfigured slideshow images are saved (relative urls)
    * @type {Array}
    */
	this.configSlideshowImages = [];

	/**
    * Array - Where the remote slideshow images are saved (remote urls)
    * @type {Array}
    */
	this.remoteSlideshowImages = [];
};



/**
 * Initialises some bits of data for the model: sets up (and initialises) the Picasa service and the Configuration
 */
com.rehabstudio.googlebooth.Model.prototype.init = function( )
{
	var self = this;
	this.configService.fetch();

	this.configService.onResult.attach(
		function(sender,response)
		{
			if(response.message == "config_success")
			{
				self.eventConfig = response.result;
				self.languageService.fetch();
				self.configSlideshowImages = self.eventConfig.slideshow;

			}
		}
	);

	this.languageService.onResult.attach(
		function(sender,response)
		{
			if(response.message == "config_success")
			{
				self.languageConfig = response.result;
				self.initPicasaService();
				self.initSlideshowService();
				self.modelEvent.notify({"message": "model_init_complete"});
			}
		}
	);

}
/**
 * Reset Application - clean the locally held data (user images)
 */
com.rehabstudio.googlebooth.Model.prototype.resetApp = function()
{

	this.cleanData();
	this.modelEvent.notify({'message':'reset_app'});

}


/**
 * Init Picasa Service, attachs all callbacks to Picasa notifications
 */
com.rehabstudio.googlebooth.Model.prototype.initSlideshowService = function( )
{

	this.slideshowService.setAjaxParameters(this.eventConfig.appBaseURL,this.eventConfig.appGUID);
	var self = this;
	this.slideshowService.onResult.attach( function(sender, data )
	{
		self.onSlideshowServiceEvent(sender, data );
	});
}

/**
 *
 */
com.rehabstudio.googlebooth.Model.prototype.fetchNewSlideshowImages = function()
{
	this.slideshowService.fetchImages();
}

/**
 *
 */
com.rehabstudio.googlebooth.Model.prototype.onSlideshowServiceEvent = function(sender, response)
{
	switch(true)
	{
		case response.message == "fetch_slideshow_error":
			this.modelEvent.notify({'message':'slideshow_images_updated'});
		break;
		case response.message == "fetch_slideshow_success":
			this.remoteSlideshowImages = response.result.slideshow;
			this.modelEvent.notify({'message':'slideshow_images_updated'});
		break;
	}

}



/**
 * Initialise all responders (Observers) to FS Service notifications, mainly split between FS init callbacks and Upload-related notifications.
 * Finally invokes the init method on the Service itself which will setup the members it is composed of
 */
com.rehabstudio.googlebooth.Model.prototype.initFSService = function()
{

	var self = this;
	this.fsService.FSEvent.attach( function(sender, data )
	{
		self._onFSServiceEvent(sender, data );
	});

	this.observeFileSystemServiceProcess();
	this.fsService.init();

}

/**
 * Init Picasa Service, attachs all callbacks to Picasa notifications
 */
com.rehabstudio.googlebooth.Model.prototype.initPicasaService = function( )
{

	this.remote.setAjaxParameters(this.eventConfig.appBaseURL,this.eventConfig.appGUID);
	var self = this;
	var userDir = null;
	var token = null;

	this.remote.picasaEvent.attach(
		function(sender,args)
		{
			switch(true)
			{
				// @about Ajax successful upload OR Ajax duplicate aborted - in both case the user is as good as sent to the back end successfully (backend notify of duplicate only if previous upload successful)

				case (args.msg === 'user_upload_success' || args.msg === 'user_upload_duplicate'):

					if(args.msg === 'user_upload_duplicate'){
						self.fsRemoveDirectory(userDir);
						self.logToFs(userDir,'user_upload_duplicate');
					}
					userDir = self.buildUserTokenName(args.userEmail,args.token);
					// @about log successful upload
					Logger.debug('@@@Ajax upload : '  + args.msg + ' from Picasa remote, we now start cleaning things up ...');
						// @about Now we need to settle the scores with the internal Matrix
						// @about Make sure we have the user in the current batch of users records in the Matrix
						var userscount = 0;

						for(var xdb in self.usersImagesMatrix.users)
						{
							userscount += 1;
						}

						Logger.debug(args.msg + ' COUNT ----- users in Matrix - ' + userscount);
						Logger.debug(args.msg + ' COUNT ----- users in self.usersImagesMatrix.usersTotal - ' + self.usersImagesMatrix.usersTotal);

						if(typeof(self.usersImagesMatrix.users['"' + userDir + '"']) != 'undefined'){
							// @about Add the record to the Matrix count of records sent
							self.usersImagesMatrix.users['"' + userDir + '"'].userdata.sent++;
							Logger.debug('User : ' +  userDir + ' | Parsed : ' + self.usersImagesMatrix.users['"' + userDir + '"'].userdata.sent + ' | Total : ' + self.usersImagesMatrix.users['"' + userDir + '"'].userdata.imagesNum);
							// @about Check if all images from this user have been sent
							if(self.usersImagesMatrix.users['"' + userDir + '"'].userdata.sent == self.usersImagesMatrix.users['"' + userDir + '"'].userdata.imagesNum){
								if(args.msg === 'user_upload_success'){
									self.logToFs(userDir,'user_upload_success');
								}
								self.fsRemoveDirectory(userDir);
								if(self.usersImagesMatrix.users['"' + userDir + '"'].userdata.realtime){
									// @about If so
									// @about If upload was part of a realtime process, kill the realtime process because we are finished (one user only). If there is a concurrent cron running it will deal with its own flag
									self.killSendProcess('realtime');
									self.modelEvent.notify({'message':'send_realtime_complete'});
									Logger.debug('OK to restart, we just killed a realtime finished thread');
								} else{
									// @about If so, 2) [case non-realtime upload] add to the Matrix count of total users parsed and delete the user folder
									self.usersImagesMatrix.usersParsed++;

									Logger.debug('USER PARSED and sent :: ' + userDir);
								}
							}
						}


						Logger.debug('MODEL - success callback - got users parsed = ' + self.usersImagesMatrix.usersParsed + ' | users total = ' + self.usersImagesMatrix.usersTotal);
						// @about Check if all users have been parsed. If so, we are done and can restart the Cron rocess!
						// NOTE usersImagesMatrix.usersParsed andusersImagesMatrix.usersTotal are only meaningful for the Cron process, the real time one does not affect them
						if(self.usersImagesMatrix.usersParsed == self.usersImagesMatrix.usersTotal)
						{
							self.killSendProcess();
							Logger.debug('OK to restart');
						}
						else
						{
							// @about We're noty quite done yet, wait for the next upload
						}
				break;

				case args.msg === 'user_upload_failed':
					userDir = args.userEmail + self.tokenSeparator + args.token;
					self.logToFs(userDir,'user_upload_failed');
					Logger.debug('@@@Ajax upload : user_upload_failed from Picasa remote - ' + userDir);
				break;

				case args.msg === 'error_posting_submission':
					self.takeAppOffline();
				break;
			}
		}
	);
}

/**
 * onServiceResult success function
 *
 * @param {Object} sender Reference to the sender object
 * @param {Object} response Object containing notification variables
 */
com.rehabstudio.googlebooth.Model.prototype._onFSServiceEvent = function(sender, response )
{
		var self = this;

		switch(true)
		{

			case response.message == "fs_init":
				self.fsService.createLogsFile();
				self.fsService.readLogs();

				self.fsService.FSEvent.attach(function(sender,args){
					if(args.message === 'logs_parse_complete'){
						self.logs = args.responseData;
						self.waitingForLogsRefresh = false;
						Logger.debug('Model - done responding to FSEvent logs_parse_complete');
					}
				});
				self.modelEvent.notify({"message": "fs_init"});
			break;

			case response.message == "fs_error":
				self.modelEvent.notify({"message": "fs_error"});
			break;

			case response.message == 'image_save_success':
					self.imagesSaved += 1;
					if(self.imagesSaved == self.userImages.length)
					{
						self.modelEvent.notify({'message':'save_complete'});
						self.imagesSaved = 0;
						if(self.isAppOnline)
						{
							this.uploadRealTime(response.data);
						}
					}
			break;
		}
}

/* ####################################################### DATA FUNCTIONS ############################*/

/**
 *
 *  Save user images to local FS
 *	@param {String} userEmail The current user email
 */
com.rehabstudio.googlebooth.Model.prototype.saveData = function(userEmail)
{
	var token = new Date().getTime();
	var userDir = this.buildUserTokenName(userEmail,token);

	// @about App is offline, just ask the FS Service to save images, we'll send later (if we need to send at all)
	var dataObject = {"imageData":this.userImages,  "userEmailAndToken":userDir};

	try
	{
			this.fsService.saveData(dataObject);
	}
	catch(error)
	{
			Logger.debug('CATCHED - Model -> FS -> saveData');
	}

}




/**
 *
 *  If the app is online this will try to upload to Picasa via remoteSubmitData
 *  If that fails, or if the app is offline, it will fall
 *  back to saving to local FS
 *	@param {String} userEmail The current user email
 */
com.rehabstudio.googlebooth.Model.prototype.uploadRealTime = function(userDir)
{
	var images = this.userImages;
	var count = this.userImages.length;
	var userEmail = this.emailFromDirname(userDir);
	var token = this.tokenFromDirname(userDir);

	if(this.isAppOnline)
	{
		// @about Flag the process as running - this is checked brfore the batch process start so will be a safeguard against concurrent send outs
		this.picasaRealtimePublishRunning = true;
		Logger.debug('SENDING IMAGE REALTIME -  picasaRealtimePublishRunning is now true' );
		// @about we will neeed the Matrix to keep track of notifications from the FIle System
		//        initialise it only if the cron is not running, otherwise we will cue our realtime stuff to the Cron's Matrix

		if(this.usersImagesMatrix.usersTotal !== undefined && this.usersImagesMatrix.usersTotal > 0)
		{
			//
		}else
		{
			this.initPublishMatrix();
		}
		this.populateMatrix(userDir,token,true);
		this.usersImagesMatrix.users['"' + userDir + '"'].userdata.imagesNum = this.userImages.length;
		try
		{
			// @about The app seems to be online, let's try to grab all images and send them through

			for(var i = 0;i < this.userImages.length;i++)
			{
				Logger.debug('SENDING IMAGE REALTIME - ' + ' - ' +  userEmail + ' - ' + path + ' - ' + token + ' - ' + count);
				var imageData = this.userImages[i].imageData;
				var imageTitle = this.userImages[i].id;
				var path = '/' + userDir + '/' + imageTitle;
				this.remoteSubmitData(imageData,userEmail,path,token,count);
			}

		}
		catch (error)
		{
			// @about Something went wrong: fall back to saving images for later send out ...
		}
	}

}



/**
 *
 * Entry point for the Picasa upload batch process: it settles / checks on the flags for traffic handling, starts the Upload process by asking tthe FS Service to read through
 * directories and generate notifications for existing (potentially unprocessed) user directories and files
 *
 */
com.rehabstudio.googlebooth.Model.prototype.publishData = function()
{
	Logger.debug('Model :: publishData :: publish flags :: waitingForLogsRefresh = ' + this.waitingForLogsRefresh + ' | picasaPublishRunning = ' + this.picasaPublishRunning + ' | picasaRealtimePublishRunning = ' + this.picasaRealtimePublishRunning);

	if(!this.waitingForLogsRefresh && !this.picasaPublishRunning && !this.picasaRealtimePublishRunning)
	{
		this.initPublishMatrix();
		this.modelEvent.notify({'message':'data_publish_start'});
		this.fsService.refreshDirectories({'cron':true});
	}
}

/**
 * Cleans User Images data - resets local array userImages
 */
com.rehabstudio.googlebooth.Model.prototype.cleanData = function()
{
	this.userImages = [];
}

/* ####################################################### GETTERS API ############################*/

/**
 * Getter - returns app watermark from app settings
 */
com.rehabstudio.googlebooth.Model.prototype.getWaterMark = function()
{
	if(!this._waterMark || this._waterMark === undefined || this._waterMark === null)
		this._waterMark = new Image();

	this._waterMark.src = this.eventConfig.waterMark;
	return this._waterMark;
}

/**
 * Getter - returns app sponsors from app settings
 */
com.rehabstudio.googlebooth.Model.prototype.getSponsors = function()
{
	if(!this._sponsors || this._sponsors === undefined || this._sponsors === null)
		this._sponsors = new Image();
	this._sponsors.src = this.eventConfig.headerSponsors;
	return this._sponsors;
}

/**
 * Getter - returns secondary branding image from app settings
 */
com.rehabstudio.googlebooth.Model.prototype.getSecondaryBranding = function()
{
	//if(!this._secbranding || this._secbranding === undefined || this._secbranding === null)
	//	this._secbranding = new Image();
	//this._secbranding.src = this.eventConfig.headerSecondary;
	return null;//this._secbranding;
}

/**
 * Getter - returns app unique ID from app settings
 */
com.rehabstudio.googlebooth.Model.prototype.getAppGUID = function()
{
	//if(!this.appGUID || this.appGUID === undefined || this.appGUID === null)
	return this.eventConfig.appGUID;
}

/**
 * Getter - returns app base url from app settings
 */
com.rehabstudio.googlebooth.Model.prototype.getAppBaseURL = function()
{
	//if(!this.appBaseURL || this.appBaseURL === undefined || this.appBaseURL === null)
	return this.eventConfig.appBaseURL;
}

/**
 * Getter - returns max photos per submission from app settings
 */
 com.rehabstudio.googlebooth.Model.prototype.getMaxPhotos = function()
 {
 	return this.eventConfig.maxPhotos;
 }

/**
 * Getter - returns app slideshow from app settings
 */
com.rehabstudio.googlebooth.Model.prototype.getSlideshow = function()
{
	return this.configSlideshowImages;
}
/**
 * Getter - returns app slideshow from cms
 */
com.rehabstudio.googlebooth.Model.prototype.getRemoteSlideshow = function()
{
	return this.remoteSlideshowImages;
}

/**
 * Getter - returns translation objects (i.e. translations VOsa)
 */
com.rehabstudio.googlebooth.Model.prototype.getTranslation = function(lang)
{
	return this.languageConfig.language.translations[lang];
}
/**
 *  - returns true if a language is rtl
 */
com.rehabstudio.googlebooth.Model.prototype.isRtl = function(lang)
{
	return this.languageConfig.language.translations[lang].rtl === true;
}


/**
 * Getter - returns languages available
 */
com.rehabstudio.googlebooth.Model.prototype.getLanguages = function()
{

	var langs = [];
	var langLen = this.languageConfig.language.translations.length;
	var xxx = this.languageConfig.language.translations;

	for(var language in this.languageConfig.language.translations)
	{
		langs.push(language);

	}

	this.currentLang = this.languageConfig.language.appDefault;

	return {'languages':langs,'default':this.languageConfig.language.appDefault};
}

/**
 * Getter - returns the app default translation
 */
com.rehabstudio.googlebooth.Model.prototype.getDefaultLanguage = function(lang)
{
	return this.languageConfig.language.appDefault;
}



/**
 * Getter - returns app terms and conditions from app settings
 */
com.rehabstudio.googlebooth.Model.prototype.getTermsAndConditions = function()
{
	return {'termsconditions':this.languageConfig.language.translations[this.languageConfig.language.appDefault].termsconditions};
}

/* ####################################################### PRIVATE HELPERS ############################*/

/**
 * Log a transaction result to the File System as notified back from the various Services (mostly Picasa)
 *
 * @param {String} userDir Reference to uaser directory, a unique user identifier formed by email and a timestamp. In real time sendout it is generated only for the purpose of logging, otherwise it is used for the user directory name
 * @param {String} resultType Reference to the upload / process result (success / error ....). At present this is just a string, no limitations or global settings exist for it
 */
com.rehabstudio.googlebooth.Model.prototype.logToFs = function(userDir,resultType)
{
	try
	{
		// @about The Service adds one string per time to the logs, so build user id and transaction result into a string
		this.fsService.addLogEntry(resultType + ':' + userDir);
		// @about After adding the log we want to be notified back from the FS Service of the new logs composition. This flag will hold back the batch cron process, as that process needs a fresh version of the logs
		this.waitingForLogsRefresh = true;
	}
	catch(error)
	{
		Logger.debug('CATCHED - Model -> FS -> logToFs');
	}
}

/**
 * Checks if a particular upload has already been logged: upload is identified by user plus resul type, the same format passed when logging, to avoid duplications
 *
 * @param {String} userDir Reference to uaser directory, a unique user identifier formed by email and a timestamp. In real time sendout it is generated only for the purpose of logging, otherwise it is used for the user directory name
 * @param {String} resultType Reference to the upload / process result (success / error ....). At present this is just a string, no limitations or global settings exist for it
 */
com.rehabstudio.googlebooth.Model.prototype.logExistsInFs = function(userDir,resultType)
{
	return(this.logs.indexOf(resultType + ':' + userDir) != -1);
}

/**
 * Asks the FS Service to remove a user directory - this is generally done either after a successful upload or when (and if) the system finds an empty user directory (unlikely)
 *
 * @param {String} currUserDir Directory to be removed. Does not need path information, just the directory name (composed of user email plus timestamp originally generated for it)
 */
com.rehabstudio.googlebooth.Model.prototype.fsRemoveDirectory = function(currUserDir)
{
	try
	{
		this.fsService.deleteUserDirectory(currUserDir);
	}
	catch(error)
	{
		Logger.debug('CATCHED - Model -> fsRemoveDirectory');
	}
}

/**
 * Attaches all needed callbacks to FS events dealing with the image send out process, from the initial directories listing all the way down to single image read / send
 * See inline comments for detailed description of the whiole process
 */
com.rehabstudio.googlebooth.Model.prototype.observeFileSystemServiceProcess = function()
{
	var self = this;
	this.fsService.FSEvent.attach(
		function(sender,args){
			var currUserDir = null;
			var userEmail = null;
			var token = null;
			var successlogs;
			var failedLogs;
			var emptyDirLogs;
			var duplicateUploadLogs;

			switch(true)
			{
				case args.msg === 'cron_directories_parsed':
						Logger.debug('Model :: cron_directories_parsed');
						// @about Start by blocking all other attempts to run a separate batch, we want one at the time
						self.picasaPublishRunning = true;
						// @about sort out what directories to send and sort logs
						var usersDirectories = args.responseData;
						successlogs = self.filterLogsBy('user_upload_success');
						failedLogs = self.filterLogsBy('user_upload_failed');
						emptyDirLogs = self.filterLogsBy('error_empty_dir');
						duplicateUploadLogs = self.filterLogsBy('user_upload_duplicate');

						if(usersDirectories.length === undefined || !usersDirectories.length)
						{
							// @about If there are no directories, we just exit and restart the cron
							self.killSendProcess();
							Logger.debug('OK to restart - no directories found');
							return false;
						}
						for(var i = 0;i<usersDirectories.length;i++)
						{
							// @about Go through the directories list after filtering and reading nly the successful logs
							currUserDir = usersDirectories[i];
							userEmail = self.emailFromDirname(currUserDir);
							token = self.tokenFromDirname(currUserDir);

							if(inArray(currUserDir,duplicateUploadLogs))
							{
								self.fsRemoveDirectory(currUserDir);
							}

							if(inArray(currUserDir,emptyDirLogs))
							{
									//  @about The user generated an empty directory, delete it
									self.fsRemoveDirectory(currUserDir);
									Logger.debug('------------ tryin to delete empty dir!!!');

							}
							else if((!inArray(currUserDir,successlogs) && !inArray(currUserDir,self.usersImagesMatrix.users)) || inArray(currUserDir,failedLogs))
							{
									// @about	Ok to add this one to the send cue - it has never been sent successfully and has not been added to cue yet
									self.populateMatrix(currUserDir,token,false);
									try
									{
										self.fsService.getUserImages(currUserDir);
									}
									catch(error)
									{
										Logger.debug('CATCHED - Model -> FS -> getUserImages');
									}

									Logger.debug('DDDD Model :: End of :: directories_parsed callback -  added  ' + currUserDir + ' - total is ' + self.usersImagesMatrix.usersTotal);
							}
							else
							{

							}
						}

						// @about If after the whole check the Matrix still has no users, it means we have nothing to process
						if(!self.usersImagesMatrix.users.length)
						{
							self.killSendProcess();
							Logger.debug('OK to rstart - Nothing to do here');
						}
				break;

				case args.msg === 'user_images_parsed':
					// @about We have records from the FS service of a directory (user) contents (images)
					var arrImagesParams = args.responseData.arrImages;
					var arrImagesParamsLen = args.responseData.arrImages.length;
					currUserDir = args.responseData.userDir;
					Logger.debug('----- ' + currUserDir + ' ------- has num images :: ' + arrImagesParamsLen);
					// @about If the dir is empty this user has no images for whatever reason: add to parsed to level ther scores and log the error ...

					if(!arrImagesParamsLen)
					{
						self.usersImagesMatrix.usersParsed++;
						// @about The next Cron will delete the directory ...
						self.logToFs(currUserDir,'error_empty_dir');
					}
					else
					{
						// @about Otherwise, loop through the directory's images to add items to the Matrix user record and read the image (parse to binary string)
						for(var z = 0;z < arrImagesParams.length;z++)
						{
							var imagePath = arrImagesParams[z].path;
							if(self.usersImagesMatrix.users !== undefined && self.usersImagesMatrix.users['"' + currUserDir + '"'] !== undefined)
							{
								// @about Increment the count of how many images this user has
								self.usersImagesMatrix.users['"' + currUserDir + '"'].userdata.imagesNum++;
								if(!inArray(imagePath,self.usersImagesMatrix.users['"' + currUserDir + '"'].userdata.images))
								{
									// @about If this image has not yet been parsed (read into binary), parse it
									try
									{
										self.fsService.readImage(imagePath);
									}
									catch(error)
									{
										Logger.debug('CATCHED - Model -> FS -> readImage');
									}
								}
							}
						}
					}
				break;

				case args.msg === 'user_image_parsed':

					// @about An image has been parsed: see who it belongs to, settle the Matrix records of total parsed for user, then send it out to Picasa
					currUserDir = args.responseData.userDir;
					userEmail = self.emailFromDirname(currUserDir);
					self.addUserImageParsed(currUserDir);

					self.remoteSubmitData(
							args.responseData.imageData,
							userEmail,
							args.responseData.fullPath,
							self.usersImagesMatrix.users['"' + currUserDir + '"'].userdata.token,
							self.usersImagesMatrix.users['"' + currUserDir + '"'].userdata.imagesNum
					);

					Logger.debug('--- Image sent to Picasa remote from Model. Image is  :: ' + args.responseData.fullPath);
				break;

				case args.msg === 'directory_delete_success':
					// @about A user dir was successfully deleted, adda log
					currUserDir = args.data;
					self.logToFs(currUserDir,'directory_delete_success');
				break;

				case args.msg === 'directory_delete_failed':
					// @about A user dir was successfully deleted, adda log
					currUserDir = args.data;
					self.logToFs(currUserDir,'directory_delete_success');
				break;

			}
		}
	);
}

/**
 * Utility method, builds a string to be used as user identifier, from the user email, the locally saved token separator (a fixed string) and a token (a datetime at present)
 *
 * @param {String} userEmail The user email
 * @param {String} token The token (a datetime generated fo the user)
 */
com.rehabstudio.googlebooth.Model.prototype.buildUserTokenName = function(userEmail,token)
{
	return userEmail + this.tokenSeparator + token;
}

/**
 * Send process helper, invokes the submitData methodf of the Picasa remote object
 *
 * @param {String} image The image binary data
 * @param {String} email The user email
 * @param {String} token The token (a datetime generated fo the user)
 * @param {String} count The amount of images that will be part of this upload (the backend needs to know so it does not have to hang on waiting idle)
 */
com.rehabstudio.googlebooth.Model.prototype.remoteSubmitData = function(image,email,path,token,count)
{
	this.remote.submitData({
		'image':image,
		'userEmail':email,
		'filePath':path,
		'token':token,
		'count':count,
		'lang':this.currentLang
	});
}

/**
 * Send process helper: extracts email address from the email+token combinations (usually sent back and forward within notifications from the Services)
 *
 * @param dirName {String} The email+token combination
 */
com.rehabstudio.googlebooth.Model.prototype.emailFromDirname = function(dirName)
{
	return dirName.substring(0,dirName.indexOf(this.tokenSeparator));
}

/**
 * Send process helper: extracts the token (Datetime string previously generated) from the email+token combinations (usually sent back and forward within notifications from the Services)
 *
 * @param dirName {String} The email+token combination
 */
com.rehabstudio.googlebooth.Model.prototype.tokenFromDirname = function(dirName)
{
	return dirName.substring(dirName.indexOf(this.tokenSeparator)+1,dirName.length);
}

/**
 * Send process helper: kills the (Cron) send processs by setting to false the internal picasaPublishRunning flag and sending out a notification to restart (which will be picked upo by Controller)
 */
com.rehabstudio.googlebooth.Model.prototype.killSendProcess = function(whatprocess)
{

	var whatprocess = whatprocess || 'cron';

	if(whatprocess == 'cron')
	{
		this.picasaPublishRunning = false;
		this.modelEvent.notify({'message':'batch_exit_restart'});
	}
	else if(whatprocess == 'realtime')
	{
		this.picasaRealtimePublishRunning= false;
	}
	else if(whatprocess == 'all')
	{
		this.picasaPublishRunning= false;
		this.picasaRealtimePublishRunning= false;
	}

}

/**
 * Send process helper, increases the count of images parsed for a give nuser
 *
 * @param {String} currUserDir The user identifier (email+token combination)
 */
com.rehabstudio.googlebooth.Model.prototype.addUserImageParsed = function(currUserDir)
{
	if(this.usersImagesMatrix.users['"' + currUserDir + '"'] !== undefined)
		this.usersImagesMatrix.users['"' + currUserDir + '"'].userdata.parsed++;
}

/**
  * Send process helper: populates the Matrix with init values for a user (representing a fresh user). If the process is realtime, the usersTotal property increase is skipped to avoid interfering with the Cron process in case a
 *
 * @param {String} userDir The user identifier (email+token combination)
 * @param {String} token   The token for the send process (same as the one used in the user identifier
 * @param {Bool}   isRealTime A flag inbdicating if this user is part of a Cron process or being sent out in real time (On Submit)
 */
com.rehabstudio.googlebooth.Model.prototype.populateMatrix = function(userDir,token,isRealTime)
{
	var arrEmpty = [];
	this.usersImagesMatrix.users['"' + userDir + '"'] = {};
	this.usersImagesMatrix.users['"' + userDir + '"'].userdata = {'images':arrEmpty,'imagesNum':0,'parsed':0,'sent':0,'token':token,'realtime':isRealTime};

	// @about Realtime submissions are ok to be added to the Matrix but the realtime process does not interfere with the cron in terms of Users processing count.
	if(!isRealTime)
		this.usersImagesMatrix.usersTotal++;

}

/**
 * Send process helper: clean internal records keeper (vo) for user images remote Picasa publishing and to keep track (due to async nature) of the FS Service notifications: by
 * matching FS Service parameters in notifications messages with MAtrix members attributes, the Model will be able to tell at what point of the process it is and what actions need to be undertaken, if any
 */
com.rehabstudio.googlebooth.Model.prototype.initPublishMatrix = function( )
{

	//Data Object for cron jons
	this.usersImagesMatrix = {};
	this.usersImagesMatrix.users = {};
	this.usersImagesMatrix.usersParsed = 0;
	this.usersImagesMatrix.usersTotal = 0;

}

 /**
 * Send process helper: logs filtering on result base, to get arrays of log entries filtered i.e. by success, or empty_dirs, or upload failure and so on.
 * It is used at the beginning of the batch upload process after receiving notification of existing users folders, so the Model knows what too do with each folder returned
 *
 * @param {String} logtype The log type seed, i.e. user_upload_success | user_upload_failed | error_empty_dir
 */
com.rehabstudio.googlebooth.Model.prototype.filterLogsBy = function(logtype)
{
	var arrFilteredLogs = [];

    if(this._logs === undefined || typeof(this._logs.length) == 'undefined')
		return arrFilteredLogs;

    if(this._logs !== undefined && this._logs.length !== undefined && this._logs.length > 0)
    {

		var logsitemsnum = this._logs.length;

		for (var i = 0;i < logsitemsnum;i++)
		{
			var logitem = this.logs[i];
			if(logitem.indexOf(logtype) === 0)
			{
				arrFilteredLogs.push(logitem.substring(logtype.length + 1,logitem.length));
			}
		}
	}

	return arrFilteredLogs;

}

/**
 * Create an image VO from the passed in binary data and stores it in the local userImages array. VO contains the image data and a fresh Timestamp
 *
 * @param {String} imageData Binary image representation
 */
com.rehabstudio.googlebooth.Model.prototype.pushImage = function(lowResData, hiResData )
{
	var newImageId = Math.round(new Date().getTime() / 1000);
	var newImage = new com.rehabstudio.googlebooth.vo.ImageVo(lowResData, hiResData, newImageId);
	this.userImages.push(newImage);
	this.modelEvent.notify({"message": "image_added_to_model", "newImage":newImage});
	this.modelEvent.notify({"message": "images_updated", "totalImages":this.userImages.length});

}

/**
 * Deletes an image VO from the local userImages array.
 *
 * @param {Object} imageVo The imageVO to remove
 */
com.rehabstudio.googlebooth.Model.prototype.removeImage = function(imageVo )
{
	//TODO loop throught _useImages to find index
	var index = $j.inArray(imageVo, this.userImages);

	if(index<0)
	{
		this.modelEvent.notify({"message": "image_not_found", "imageId":imageVo.id});
		return;
	}
		var removedId = imageVo.id;
		this.userImages.splice(index,1);
		this.modelEvent.notify({"message": "image_removed", "imageId":imageVo.id});
		this.modelEvent.notify({"message": "images_updated", "totalImages":this.userImages.length});

}


/**
 * Deletes an image VO from the local userImages array.
 *
 * @param {Object} imageVo The imageVO to remove
 */
com.rehabstudio.googlebooth.Model.prototype.takeAppOffline = function( )
{
	this.isAppOnline = false;
	this.killSendProcess('all');
	this.usersImagesMatrix = {};
	this.remote.resetSubmissionQue();
}