
/**
 * View component of the Model View Controller implementation
 * @param {Object} dom References to DOM objects used in this view
 * @constructor
 */
com.rehabstudio.googlebooth.View = function(dom)
{

	var self = this;

	this._dom = dom;
	this._mediator = {};

	self.allowClick = true;

	this.header = new com.rehabstudio.googlebooth.view.Header(this._dom.body.find("#header"));
	this.footer = new com.rehabstudio.googlebooth.view.Footer(this._dom.body.find("#footer"));
	this.bg = this._dom.body.find("#bg");

	this.standByView = new com.rehabstudio.googlebooth.view.StandByView(this._dom.body.find("#standby-view"));
	this.photobooth = new com.rehabstudio.googlebooth.view.Booth(this._dom.body.find("#booth-view"));
	this.userImages = new com.rehabstudio.googlebooth.view.UserImagesView(this._dom.body.find("#user-images-view"));
	this.submitView = new com.rehabstudio.googlebooth.view.SubmitView(this._dom.body.find("#submit-view"));
	this.thanksView = new com.rehabstudio.googlebooth.view.ThanksView(this._dom.body.find("#thanks-view"));

	this.ViewEvent = new com.rehabstudio.Event( this );

	this.touchAnimPosition = 0;
	this.touchAnimSteps = 17;

	this._particular = this._dom.body.find("#particular");

	$j.Body.mousedown(
		function(e)
		{
			if(e.target.id != 'termsModalBody')
				self._animateBGparticle(e.pageX,e.pageY);
		}
	);

	this.currentView = null;
	this.viewID = {
		'standBy':'STANDBY',
		'booth':'BOOTH',
		'submit':'SUBMIT',
		'thanks':'THANKS'
	};

}



com.rehabstudio.googlebooth.View.prototype._animateBGparticle = function(mousex,mousey)
{
	var self = this;

	if(this._intParticularFade) return;

	this._resetParticular();

	this._particular.css("left",mousex - 45 +'px');
	this._particular.css("top",mousey - 45 +'px');


		this._intParticularFade = setInterval(
		function()
		{

			self.touchAnimPosition += 1;
			self._particular.css("backgroundPosition", 81-(self.touchAnimPosition*81)+"px");

			if(self.touchAnimPosition == self.touchAnimSteps)
				self._resetParticular();

		},30);
}


com.rehabstudio.googlebooth.View.prototype._resetParticular = function(mousex,mousey)
{


	this._particular.css("left", '-100px');
	this._particular.css("top", '-100px');
	this.touchAnimPosition = 0;
	this._particular.css("backgroundPosition", "81px");

	if(this._intParticularFade)
		clearInterval(this._intParticularFade);

	this._intParticularFade = null;

}



/**
 * Execute
 */
com.rehabstudio.googlebooth.View.prototype.init = function()
{
	//Initialize booth
}


/**
 * Start App
 */
com.rehabstudio.googlebooth.View.prototype.startApp = function()
{
	this.setToStandBy();
}


/**
 * Start App
 */
com.rehabstudio.googlebooth.View.prototype.resetApp = function()
{
	this.userImages.resetView();
	this.photobooth.resetView();
	this.submitView.resetView();
	this.setToStandBy();
}


/**
 * Set App to standby
 */
com.rehabstudio.googlebooth.View.prototype.setToStandBy = function()
{
	this.submitView.hide();
	this.standByView.show();
	this.photobooth.hide();
	this.userImages.hide();
	this.thanksView.hide();
	this.footer.hide();
	this.currentView = this.viewID.standBy;
}

/**
 * Set App to camera
 */
com.rehabstudio.googlebooth.View.prototype.setToCamera = function()
{
	this.submitView.hide();
	this.standByView.hide();
	this.photobooth.show();
	this.userImages.show();
	this.userImages.setToColumns();
	this.thanksView.hide();
	this.footer.show();
	this.currentView = this.viewID.booth;
}


/**
 * Set App to submit
 */
com.rehabstudio.googlebooth.View.prototype.setToSubmit = function()
{
	this.submitView.show();
	this.standByView.hide();
	this.photobooth.hide();
	this.userImages.show();
	this.userImages.setToRow();
	this.thanksView.hide();
	this.footer.show();
	this.currentView = this.viewID.submit;
	//start with standBy view and wait until first touch
}


/**
 * Set App to thanks
 */
com.rehabstudio.googlebooth.View.prototype.setToThanks = function()
{
	this.submitView.hide();
	this.standByView.hide();
	this.photobooth.hide();
	this.userImages.hide();
	this.thanksView.show();
	this.footer.show();
	this.currentView = this.viewID.thanks;
	//start with standBy view and wait until first touch
}


/**
 * Show "Take Photo" Button
 */
com.rehabstudio.googlebooth.View.prototype.mediateView = function(mediator)
{
	this._mediator = mediator;
}



/**
 * Resize Function
 */
com.rehabstudio.googlebooth.View.prototype.resize = function(wid, hei)
{

	var ratio = 683/768;

	this.bg.css("width", wid+"px");
	this.bg.css("height", hei+"px");

	var shard = this.bg.find(".shard");
	shard.css("height", hei+"px");
	shard.css("width", hei*ratio+"px");

	this.thanksView.resize(wid, hei);
	this.userImages.resize(wid, hei);
	this.photobooth.resize(wid, hei);
	this.standByView.resize(wid, hei);
	this.submitView.resize(wid, hei);
}

com.rehabstudio.googlebooth.View.prototype.translate = function(translator)
{
	this.submitView.translate(translator);
	this.thanksView.translate(translator);
	this.photobooth.translate(translator);
	this.userImages.translate(translator);
	this.submitView.translate(translator);
	this.standByView.translate(translator);
	this.footer.translate(translator);
}

