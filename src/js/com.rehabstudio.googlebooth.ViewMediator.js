
/**
 * Mediator component of the Model View Controller implementation
 * @param {*} model The model for this view
 * @param {*} controller The controller for this view
 * @param {Object} dom References to DOM objects used in this view
 * @constructor
 */
com.rehabstudio.googlebooth.ViewMediator = function( model, view, controller )
{

	this._model = model;
	this._controller = controller;
	this._view = view;

}


/**
 * Execute
 */
com.rehabstudio.googlebooth.ViewMediator.prototype.execute = function()
{
	var self = this;

	/*
	*
	* Standby view listeners
	*
	*/

	this._view.standByView.standByEvent.attach( function(sender, response) {

		self._controller.resetStandByTimer();

		//Logger.debug(response.message);

		switch(true){

			//STAND BY VIEW CLICK - PROCEED TO CAMERA
			case response.message === "standby_start_click":
				self._view.setToCamera();
				break;

			//STAND BY VIEW - VISIBLE
			case response.message === "standbyview-visible":
				self.setDefaultLang();
				break;
			//STAND BY VIEW - HIDDEN
			case response.message === "standbyview-hidden":
				break;
			case response.message === "config-images-loaded":
				self._controller.initRemoteImagesService();
				break;


			default:
		}

	});

	/*
	*
	*Add Booth Listeners
	*
	*/
	this._view.photobooth.boothEvent.attach( function(sender, response) {

		self._controller.resetStandByTimer();

		switch(true){

			//CAMERA IS READY - INIT FS SERVICE
			case response.message === "camera_ready":
				self._model.initFSService();
				break;

			//WATERMARK IS READY - DEPRECATED - COMES WITH MODEL INIT COMPLETE
			case response.message === "watermark_ready":
				break;

			//CAMERA INIT ERROR
			case response.message === "camera_error":
				break;

			//INITIATE COUNTDOWN
			case response.message === "init_countdown":
				break;
			//PHOTO IS CAPTURED - SEND TO MODEL
			case response.message === "photo_captured":
				self.addImageToModel(response.lowResData, response.hiResData);
				break;
			//PHOTO IS TO BE SUBMITTED - SWITCH VIEWS
			case response.message === "submit_images_click":
				self._view.setToSubmit();
				break;

			default:
				Logger.debug(response.message);
		}

	});


	/*
	*
	*Add User Images View Listeners
	*
	*/
	this._view.userImages.userImagesEvent.attach( function(sender, response) {

		self._controller.resetStandByTimer();

		switch(true){

			//REMOVE IMAGE IS FIRED
			case response.message === "remove_image":
				self._model.removeImage(response.imageVo);
				break;
		}

	});




	/*
	*
	*Add Model Event Listeners
	*
	*/
	this._model.modelEvent.attach( function(sender, response) {



		switch(true){

			//MODEL IS READY
			case response.message === "model_init_complete":
				self._view.photobooth.init(self._model.getWaterMark(), self._model.getMaxPhotos());
				self._view.header.setTopImages({
					sponsor: self._model.getSponsors(),
					secondary: self._model.getSecondaryBranding()
				});
				self._view.footer.makeLangButtons(self._model.getLanguages());
				self._view.submitView.init(self._model.getTermsAndConditions())
				self._view.translate(
					self.buildTranslator(
						self._model.getDefaultLanguage()
					)
				);
				self._view.standByView.setSlideshow(self._model.getSlideshow());
				break;

			//FILE SYSTEM IS READY
			case response.message === "slideshow_images_updated":
				Logger.debug("in mediator: slideshow_images_updated");
				self._controller.resetStandByTimer();
				self._view.standByView.setRemoteSlideshow(self._model.getRemoteSlideshow());
				break;

			//IMAGE ADDED TO THE MODEL
			case response.message === "image_added_to_model":
				self._controller.resetStandByTimer();
				self._view.userImages.pushImage(response.newImage);
				break;

			//IMAGES UPDATED
			case response.message === "images_updated":
				self._controller.resetStandByTimer();
				self._view.photobooth.imagesUpdated(response.totalImages);
				break;

			//IMAGE REMOVED FROM THE MODEL
			case response.message === "image_removed":
				self._controller.resetStandByTimer();
				self._view.userImages.removeImage(response.imageId);
				break;

			//FILE SYSTEM IS READY
			case response.message === "fs_init":
				self.startApp();
				break;

			//FILE SYSTEM INIT ERROR
			case response.message === "fs_error":
				break;

			//SAVE COMPLETE
			case response.message === "save_complete":
				self._controller.resetStandByTimer();
				setTimeout(function(){
					self._model.resetApp();
				},4000);
				break;

			//SAVE FAILED
			case response.message === "save_failed":
				break;

			//RESET APPLICATION
			case response.message === "reset_app":
				self.resetApp();
				break;

			default:
		}

	});




	/*
	*
	*	Submit View Listeners
	*
	*/
	this._view.submitView.sendImagesEvent.attach(function(sender,args){

		self._controller.resetStandByTimer();

		switch(true){
			case args.message === "submit_view_initialised":
				//do nothing
				break;
			case args.message === "submit_view_showed":
				//do nothing
				break;
			case args.message === "submit_view_exit":
				//do nothing
				break;
			case args.message === "user_submit_success":
				var userEmail = args.data;
				self._model.saveData(userEmail);
				self._view.setToThanks();
				self._view.userImages.imagesSubmitted();
				break;
			case args.message === 'submit_rewind':
				self._view.setToCamera();
				break;
			default:
		}

	});


	/*
	*
	*	Footer
	*
	*/
	this._view.thanksView.thanksEvent.attach(function(sender,args){

		self._controller.resetStandByTimer();

		switch(true){
			case args.message === "thanks_click_reset_please":
				self._model.resetApp();
				break;
			default:
		}

	});

	/*
	*
	*	Footer
	*
	*/
	this._view.footer.footerEvent.attach(function(sender,args){

		self._controller.resetStandByTimer();

		switch(true){
			case args.message === "reset_app":
				if(self._view.photobooth.takingPhoto == false)
				{
					self._model.resetApp();
				}
				break;
			default:
		}

	});
	this._view.footer.languageEvent.attach(function(sender,args){
		switch(true){
			case args.message === "lang_selected":
				var newLanguage = args.data;

				self._model.currentLang = newLanguage;
				self._view.translate(self.buildTranslator(newLanguage));
				break;
			default:
		}
	});

}

/**
 * Start App
 */
com.rehabstudio.googlebooth.ViewMediator.prototype.startApp = function(imageData)
{
	this._controller.startStandByTimer();
	this._view.startApp();
}


/**
 * Reset App
 */
com.rehabstudio.googlebooth.ViewMediator.prototype.resetApp = function(imageData)
{
	this._view.resetApp();
}




/**
 * Save Image
 */
com.rehabstudio.googlebooth.ViewMediator.prototype.addImageToModel = function(lowResData, hiResData)
{

	this._model.pushImage(lowResData, hiResData);

}

/**
 * Save Image
 */
com.rehabstudio.googlebooth.ViewMediator.prototype.submitImages = function()
{

	this._model.submitImages();
}

/**
 * buildTranslator
 */
com.rehabstudio.googlebooth.ViewMediator.prototype.buildTranslator = function(lang)
{
	var newTranslation = this._model.getTranslation(lang);
	var isRtl = this._model.isRtl(lang);

	return new com.rehabstudio.utils.Translator(newTranslation, isRtl);
}

/**
 * setDefaultLang
 */

com.rehabstudio.googlebooth.ViewMediator.prototype.setDefaultLang = function(){
	this._view.translate(
		this.buildTranslator(
			this._model.getDefaultLanguage()
		)
	);


	this._view.footer.defaultLangButton(this._model.getDefaultLanguage());
}
