/**
 * The root namespace
 *
 * @constructor
 */
com = {}

/**
 * The full business namespace
 *
 * @constructor
 */
com.rehabstudio = {}

/**
 * Utility classes namespace for logging, FS operations, timers ...
 *
 * @constructor
 */
com.rehabstudio.utils = {}

/**
 * The googlebooth app namespace
 *
 * @constructor
 */
com.rehabstudio.googlebooth = {}

/**
 * All view classes composing the googlebooth app
 *
 * @constructor
 */
com.rehabstudio.googlebooth.view = {}

 /**
 * Namespace for UI classes
 * @constructor
 */
com.rehabstudio.googlebooth.ui = {}

/**
 * Base widgets for UI classes to extend
 * @constructor
 */
com.rehabstudio.googlebooth.ui.widget = {}

/**
 * Remote (ajax) operations classes
 * @constructor
 */
com.rehabstudio.googlebooth.remote = {}

/**
 * Classes providing inter-mvc services
 * @constructor
 */
com.rehabstudio.googlebooth.service = {}

/**
 * Namespace for virtual objects
 * @constructor
 */
com.rehabstudio.googlebooth.vo = {}