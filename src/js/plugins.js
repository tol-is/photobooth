function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function inObject(needle, haystack) {
    for(var prop in haystack) {
        if(haystack[prop] == needle) return true;
    }
    return false;
}

/**
 * Simple Logger class for debugging
 */
var Logger = function()
{
}

/**
 * Prints a messages or object to the console
 * @param {...*} args The message(s) to log
 */
Logger.debug = function( args )
{
	return;
    if( typeof( window.console ) != "undefined" ) {
		 window.console.log( "DEBUG:", arguments );
	}
}



function randomXToY(minVal,maxVal)
{
    var randVal = minVal+(Math.random()*(maxVal-minVal));
    return Math.round(randVal);
}


