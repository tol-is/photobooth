
/**
 * Service class for the Model View Controller implementation
 * @constructor
 */
com.rehabstudio.googlebooth.remote.ConfigService = function(url)
{

    this.onResult = new com.rehabstudio.Event( this );
    this._url = url;
    this._method= "POST";
    this._dataType = "json";
    this._data = {};

    /**
     * @private
     * The results of this Service
     * @type {Object}
     */
    this._result = {};

}


/**
 * Fetch Things.
 */
com.rehabstudio.googlebooth.remote.ConfigService.prototype.fetch = function()
{
        var me = this;

        $j.ajax({
            url: this._url,
            dataType: this._dataType,
            data: this._data,
            error: function(result)
            {
                me.onResult.notify( {'message':'config_error'} );
            },
            success: function(result)
            {
                me.onResult.notify( {'message':'config_success','result':result} );
            }
        });
}



