
/**
 * Service class for the Model View Controller implementation
 * @constructor
 */
com.rehabstudio.googlebooth.remote.PicasaService = function()
{

   // this._url = "http://photobooth.rehabstudio.com/upload.php";
    //this._url = "http://googlebooth.local/upload.php";
    
    this._appGUID = "";
    this._url = "";

    this._method= "POST";
    this._dataType = "json";
    this._data = {};
    this._tokensToPksMatrix = [];
    this._splitseedChar = '_#_';

    /**
    *
    */
    this._submissionCue = {};
     /**
     * Dispatched when the request is complete
     * @type {com.rehabstudio.Event}
     */
     this.picasaEvent = new com.rehabstudio.Event( this );


    /**
     * @private
     * The results of this Service
     * @type {Object}
     */
    this._result = {};


}


/**
 * Make the ajax request
 * Deprecated
 */
com.rehabstudio.googlebooth.remote.PicasaService.prototype.setData = function(data)
{
       this._data = data;
}

/**
 * Setup ajax parameters for remote
 */
com.rehabstudio.googlebooth.remote.PicasaService.prototype.setAjaxParameters = function(url,guid)
{
       this._url = url;
       this._appGUID = guid;
}


/**
 * Upload an image to the backend cms
 *
 * @param {String}
 * @param {String}
 * @param {String}
 * @param {String}
 * @param {String}
 * @param {String}
 */
com.rehabstudio.googlebooth.remote.PicasaService.prototype.uploadImage = function(imageData,pk,submissionToken,guid,userEmail,filePath)
{
    var self = this;
    $j.ajax({
                url: self._url + guid + '/' + pk,
                type: self._method,
                dataType: self._dataType,
                data:JSON.stringify({
                    file:imageData
                }),
                success: function(result)
                {
                    self.picasaEvent.notify( {'msg':'user_upload_success','userEmail':userEmail,'token':submissionToken,'filePath':filePath});
                    Logger.debug('-- ++++++++++++++++++ PICASA SEND OUT - SUCCESS - userEmail is '  + userEmail +  '++++++++++++++++++ --');
                },
                error:function()
                {
                    self.picasaEvent.notify( {'msg':'user_upload_failed','userEmail':userEmail,'token':submissionToken,'filePath':filePath});
                    Logger.debug('Error posting image');
                    var submissionErrorCode = 'ajax_upload_error_' + submissionToken;
                    throw submissionErrorCode;
                }
    });
}


/**
 * Make the ajax request
 *
 * @param {Object} dataIn A VO containing user email and token, the imageVO for the submission, the file path and the total number of images for the associated user
 */
com.rehabstudio.googlebooth.remote.PicasaService.prototype.submitData = function(dataIn)
{

    Logger.debug('-- ++++++++++++++++++ PICASA SEND OUT - START - userEmail is '  + dataIn.userEmail +  '++++++++++++++++++ --');
    var self = this;
    
    // @about Start colecting all parameters
    var imageBinData = dataIn.image.split(',')[1];
    var userEmail = dataIn.userEmail;
    var filePath = dataIn.filePath;
    var guid = this._appGUID;
    var submissionToken = dataIn.token;
    var submissionCount = dataIn.count;
    var submissionLang = dataIn.lang;

    // @about Check if a record exists in _submissionCue (the local 'data table') for the passed user token (submissionToken)
    if(this._submissionCue[submissionToken] === undefined)
    {
        // @about If not, create a record where we will store this user's data until we have enough data to match the user images count (submissionCount)
        this._submissionCue[submissionToken] = [];
    }
    // @about Add the image data to the user record
    this._submissionCue[submissionToken].push(imageBinData);

    // @about Check if the records for the user so far match the total count
    if(this._submissionCue[submissionToken].length == submissionCount)
    {
        // @about If we have enough records in the local _submissionCue for this user, we can start the actual Ajax process by requesting a pk to the back end
        $j.ajax({
            url:self._url + guid,
            type:self._method,
            dataType: self._dataType,
            data:JSON.stringify({email:userEmail,token:submissionToken,count:submissionCount,lang:submissionLang}),
            success: function(result)
            {
                    var objResponse = result;
                    var pk = objResponse.content.pk || false;
                    // @about We had a response and are trying to get the pk from it: 
                    if(!pk)
                    {
                        // @about if it is not there, then we had a duplicate submission error from the backend
                        Logger.debug('---------------- Duplicate submission for token ' + submissionToken);
                        self.picasaEvent.notify( {'msg':'user_upload_duplicate','userEmail':userEmail,'token':submissionLang});
                    }
                    else
                    {
                        // @about All good: we have records and a pk, can do the actual upload
                        Logger.debug('---------------- Success pk return ' + pk + ' for token ' + submissionToken);
                        for(var i = 0;i < submissionCount; i++)
                        {
                            self.uploadImage(self._submissionCue[submissionToken][i],pk,submissionToken,guid,userEmail,filePath);
                        }
                        // @about Reset the records for this user for the next process, just in case the current one failed we'll be still able to retry ...
                        self._submissionCue[submissionToken] = [];
                    }
            },
            error:function()
            {
                // @about No Ajax process, possibly no connectivity though the Model thought to be online: just reset the submission que. If following submissions get connection back they will go through, 
                //        otherwise the cue will just be reset and the next Controller scheduled connectivity check will take the app offline and leave all uploads to when connection is back
                self.resetSubmissionQue();
                Logger.debug('Error posting submission info');
                self.picasaEvent.notify( {'msg':'error_posting_submission','userEmail':userEmail,'token':submissionToken});
            }
        });
    }
    else if(this._submissionCue[submissionToken].length > submissionCount)
    {
        // @about For some reason we have more records (images) then the actual submission count - reset it, something must have gone wrong, leave it to the next cron as it will just be unsent and unlogged
       this._submissionCue[submissionToken] = [];
    }
}

/**
 * Reset the local record keeping Obkect
 */
com.rehabstudio.googlebooth.remote.PicasaService.prototype.resetSubmissionQue = function()
{
         this._submissionCue = [];
}
/**
 * Make the ajax request
 */
com.rehabstudio.googlebooth.remote.PicasaService.prototype._formatPostData = function()
{
        return {'test':'test'};//this._data;
}

/**
 * Setup ajax parameters for remote
 */
com.rehabstudio.googlebooth.remote.PicasaService.prototype._submissionTokenHasPk = function(submissionToken)
{
       var matrixLen = this._pkTokensToPksMatrix.length;
       for(var i = 0;i < matrixLen;i++)
       {
            if(this._pkTokensToPksMatrix[i].indexOf(submissionToken) > -1)
                return this._pkTokensToPksMatrix[i].split(this._splitseedChar)[1];
       }
       return false;
}


