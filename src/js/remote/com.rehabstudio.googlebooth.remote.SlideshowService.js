
/**
 * Service class for the Model View Controller implementation
 * @constructor
 */
com.rehabstudio.googlebooth.remote.SlideshowService = function()
{

   // this._url = "http://photobooth.rehabstudio.com/upload.php";
    //this._url = "http://googlebooth.local/upload.php";
    
    this._appGUID = "";
    this._url = "";

    this._method= "GET";
    this._dataType = "json";
    this._data = {};
    this._tokensToPksMatrix = [];

     /**
     * Dispatched when the request is complete
     * @type {com.rehabstudio.Event}
     */
     this.onResult = new com.rehabstudio.Event( this );


    /**
     * @private
     * The results of this Service
     * @type {Object}
     */
    this._result = {};


}


/**
 * Make the ajax request
 * Deprecated
 */
com.rehabstudio.googlebooth.remote.SlideshowService.prototype.setData = function(data)
{
       this._data = data;
}

/**
 * Setup ajax parameters for remote
 */
com.rehabstudio.googlebooth.remote.SlideshowService.prototype.setAjaxParameters = function(url,guid)
{
    this._url = url;
    this._appGUID = guid;
}


/**
 * Upload an image to the backend cms
 *
 * @param {String}
 * @param {String}
 * @param {String}
 * @param {String}
 * @param {String}
 * @param {String}
 */
com.rehabstudio.googlebooth.remote.SlideshowService.prototype.fetchImages = function()
{

    var me = this;
    $j.ajax({
                url: me._url + me._appGUID + '/?slideshow',
                //url: "data/slideshow.json",
                
                type: me._method,
                dataType: me._dataType,
                data:me._data,
                error: function(result)
                {
                    me.onResult.notify( {'message':'fetch_slideshow_error'} );
                },
                success: function(result)
                {
                    me.onResult.notify( {'message':'fetch_slideshow_success','result':result} );
                }
    });
}

