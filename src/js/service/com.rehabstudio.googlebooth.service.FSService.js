
/**
 * File System Service class
 * @constructor
 */
com.rehabstudio.googlebooth.service.FSService = function()
{
  this.FSEvent = new com.rehabstudio.Event(this);
}

/**
 * Initialize
 */
com.rehabstudio.googlebooth.service.FSService.prototype.init = function()
{
  
  var self = this;
  this.fs = new com.rehabstudio.utils.FS();

  this.userOps = new com.rehabstudio.utils.User(this.fs);
  this.userOps.initialise();

  this.logOps = new com.rehabstudio.utils.Log(this.fs);
  this.logOps.initialise();

  this.fs.FSbaseEvent.attach(function(sender,args)
  {
    switch(true)
    {

      case args.message === 'fs_init':
                self.FSEvent.notify({'message':'fs_init'});
      break;

      case args.message === 'file_write_complete':
          switch (args.initiator)
          {
            case self.logOps.tokens.logswrite:
                self.FSEvent.notify({"message": "logs_write_complete", "responseData": args.responseData});
            break;
          }
      break;

      case args.message === 'file_read_complete':
          switch (args.initiator){
            case self.logOps.tokens.logsread:
              self.FSEvent.notify({"message": "logs_parse_complete", "responseData": args.responseData.split(self.logOps.logNewLine)});
            break;
            case self.userOps.tokens.userimageread:
              self.FSEvent.notify({'responseData':{'fullPath':args.fullPath,'imageData':args.responseData,'userDir':args.fullPath.substring(1,args.fullPath.lastIndexOf('/')) }, 'msg':'user_image_parsed'});
              Logger.debug('FSService - Image read complete, notified');
            break;
          }
      break;

      case args.message === 'file_save_success':
            switch (args.initiator)
            {
                case self.logOps.tokens.logscreate:
                    // skip - this might be unsuccessful if the logs are there already: FS will complain but all is ok
                break;

                case self.userOps.tokens.userimagesave:
                    self.FSEvent.notify({'data':self.userOps.getUser(),'imagePath':args.filePath,'message':'image_save_success'});
                break;
            }
      break;

      case args.message === 'folder_created':
        switch (args.initiator)
        {
          case self.userOps.tokens.userfoldercreate:
            self.FSEvent.notify({'data':args.data,'msg':'user_folder_created'});
            // @about if the User obj registered a callback for this folder creation, run it (that would be images to be saved)
            //        this can be thought of as a hard plugged one-way notification (observer) device
            self.userOps.runLocalSaveCue(args.initiator,args.data);
          break;
        }
      break;

      case args.message === 'directory_delete_success':
          switch (args.initiator)
          {
            case self.userOps.tokens.userfolderdelete:
              self.FSEvent.notify({'data':args.data,'msg':'directory_delete_success'});
            break;
          }
      break;

      case args.message === 'file_deleted':
          switch (args.initiator)
          {
             case self.userOps.tokens.userimagedelete:
              self.FSEvent.notify({'data':args.data,'msg':'file_deleted'});
            break;
          }
          
      break;

      case args.message === 'folder_content_parsed':
          switch (args.initiator)
          {
            case self.userOps.tokens.userimageslist:
              self.FSEvent.notify({'responseData':{'arrImages':args.responseData.arrFiles,'userDir':args.responseData.dirName},'msg':'user_images_parsed'});
            break;

            case self.userOps.tokens.usersfolderslist:
              self.userOps.usersFolders = args.responseData.arrDirectories;
            break;

            case self.userOps.tokens.usersfolderslistcron:
               self.FSEvent.notify({'responseData':args.responseData.arrDirectories,'msg':'cron_directories_parsed'});
            break;
          }
      break;

    }
  });
}

/* ================================================================================================ */
/* ================================================================================================ */
/* ------------------------------------------------------------- API ---------------------------------------------------- */

/*
* The function used to save users images: data must comform to specs
*
* @param  {Object} data Specs for data objectes are as follows: {email : 'user@domain.com', imageData:[ ImageVO , ImageVO , ImageVo, ImageVO]} ]
*
*/
com.rehabstudio.googlebooth.service.FSService.prototype.saveData = function(data)
{
    this.userOps.saveUserImageData(data);
}

/*
* Asks the User member to read all existing directory in the FS root (User::readUsersDirectories)  
*
* @param {Object} objOptions A VO containig some options: {isCron:Bool} Tells if this is part of a Cron process |
*
*/
com.rehabstudio.googlebooth.service.FSService.prototype.refreshDirectories = function(objOptions)
{

    if(objOptions.cron !== undefined && objOptions.cron)
    {
      this.userOps.readUsersDirectories(true);
    }
    else
    {
      this.userOps.readUsersDirectories(false);
    }
}

/*
*
* Invokes the readLogs function on the Logs member
*
*/
com.rehabstudio.googlebooth.service.FSService.prototype.readLogs = function()
{
    this.logOps.readLogs();
};

/*
* Add a log entry, calls the logEntry  function on the Logs member
*
* @param {String} logEntryText
*
*/
com.rehabstudio.googlebooth.service.FSService.prototype.addLogEntry = function(logEntryText)
{
    this.logOps.logEntry(logEntryText);
};

/*
* Returns array of images given a user email, or current _user property if userEmail is undefined
*
* @param userDir ["user@domain.com" + token]
*
*/
com.rehabstudio.googlebooth.service.FSService.prototype.getUserImages = function(userDir)
{
    this.userOps.userImagesToArray(userDir);
};

/*
* @function readImage
*
* @param fullPath ["/user@domain.com/imagetitle.imgext"]
* 
* @about Reads image data given full image path. The full path is usually returned from another FS operation
*
*/
com.rehabstudio.googlebooth.service.FSService.prototype.readImage = function(fullPath)
{
  this.userOps.readUserImage(fullPath);
}


/*
* @function deleteImage
*
* @param requireed fullPath  ["/user@domain.com#timestamp/imagetitle.imgext" ]
*
* @about Delete image: can be used with full images path or relative to either current _user property or a passed user
*
*/
com.rehabstudio.googlebooth.service.FSService.prototype.deleteImage = function(fullPath)
{
  this.userOps.deleteImage(fullPath);
}


/*
* @function deleteUserDirectory
*
* @param userEmail ["user@domain.com"]
*
* @about Recursively deletes user folder with all itas content
*
*/
com.rehabstudio.googlebooth.service.FSService.prototype.deleteUserDirectory = function(userDir)
{
  this.userOps.deleteUserDirectory(userDir);
}

/*
* @function deleteUserDirectory
*
* @param userEmail ["user@domain.com"]
*
* @about Recursively deletes user folder with all itas content
*
*/
com.rehabstudio.googlebooth.service.FSService.prototype.createLogsFile = function()
{
  this.logOps.createLogsFile();
}


