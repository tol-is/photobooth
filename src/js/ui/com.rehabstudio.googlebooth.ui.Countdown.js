/**
 * @constructor
 * @param {Object} container The flash container dom element
 */
com.rehabstudio.googlebooth.ui.Countdown = function(container)
{

	this.container = container;
	var self = this;
	
	this.container.css("opacity", 0);

	this.timerSound = null;

	this.timerSound = document.createElement('audio');
	this.timerSound.setAttribute('src', 'sounds/timer.wav');
	this.timerSound.load();


	/**
	* Dispatched on Shutter Events along with a message
	* @type {com.rehabstudio.Event}
	*/
	this.countDownEvent = new com.rehabstudio.Event( this );

}


/**
 * Countdown
 */
com.rehabstudio.googlebooth.ui.Countdown.prototype.play = function()
{

	var me = this;

	me.container.css("opacity", 1).html("3");
	//me.timerSound.play();
	
	setTimeout(function()
	{

		me.container.html("2");
		//me.timerSound.play();

			setTimeout(function()
			{

				me.container.html("1");
				//me.timerSound.play();
				
					setTimeout(function()
					{

						me.container.css("opacity", 0);
						me.countDownEvent.notify("countdownComplete");

					}, 950);

			}, 1000);

	}, 1000);

}