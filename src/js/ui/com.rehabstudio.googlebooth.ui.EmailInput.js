/**
 * @constructor
 * @param {Object} container The keyboard container dom element
 */

com.rehabstudio.googlebooth.ui.EmailInput = function()
{
}

com.rehabstudio.googlebooth.ui.EmailInput.prototype = new com.rehabstudio.googlebooth.ui.widget.TextInput();
com.rehabstudio.googlebooth.ui.EmailInput.prototype.constructor = com.rehabstudio.googlebooth.ui.EmailInput;




com.rehabstudio.googlebooth.ui.EmailInput.prototype.initialise = function(name,initVal,delegate)
{
	// @about set defaults for optional parameters
	if(initVal === undefined) initVal = this.welcomeString?this.welcomeString:'';

	// @about Invoke the parent initialise()
	com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.initialise.call(this,name,initVal,delegate);

	var validEmailRule = {};
	validEmailRule.rule = /^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$/;
	validEmailRule.msg = '';
	validEmailRule.controlFeedback = false;// -> Do not use the text to give feedback, leave to the mvc to implement that as it is subejct to translation
	this.setValidationRule(validEmailRule);
}

com.rehabstudio.googlebooth.ui.EmailInput.prototype.userEmailInput_KeyboardEventRespond = function(sender,args)
{
	if(args.message == 'key_stroke')
	{
		this.setValue(args.data);
		this.__clearState = false;
		this.clearValidationTarget();

	}
	else if(args.message == 'key_stroke_canc')
	{
		this.popValue();
	}
	
}

com.rehabstudio.googlebooth.ui.EmailInput.prototype.updateDisplayDelegate = function()
{
	this.displayDelagate.innerHTML = 
		'<span>' + 
		this.__domTextInput.getAttribute('value') + 
		'</span>' + 
		'<img src="img/blinkCursor.gif" /><span class="cl"></span>';
}