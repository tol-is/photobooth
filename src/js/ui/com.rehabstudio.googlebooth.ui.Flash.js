/**
 * @constructor
 * @param {Object} container The flash container dom element
 */
com.rehabstudio.googlebooth.ui.Flash = function(container)
{

	this.container = container;
	var self = this;
	
	this.tween = null;
	this.position = {alpha:100};

	this.container.css("opacity", 0);
}


/**
 * Flash animation
 */
com.rehabstudio.googlebooth.ui.Flash.prototype.fire = function()
{
	this.container.css("opacity", 1);
	
	var self = this;
	
	this.position.alpha=100;
	
	this.tweenshow = new TWEEN.Tween(self.position)
	.to({alpha: 0}, 600)
	.onUpdate(function()
	{
		self.container.css("opacity",(self.position.alpha/100));
	})
	.start();
}


/**
 * Resize Function
 */
com.rehabstudio.googlebooth.ui.Flash.prototype.resize = function(wid, hei)
{
	this.container.css("width", wid+"px");
	this.container.css("height", hei+"px");
}


