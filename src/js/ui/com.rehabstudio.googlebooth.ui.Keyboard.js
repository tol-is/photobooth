/**
 * All functionality related to typing within ther Booth app
 *
 * @constructor
 */
com.rehabstudio.googlebooth.ui.Keyboard = function()
{
	
}

/**
 * Initialize function: reset keyboard style, set visibility,attach callbacks
 * @param {String} name Reference to the keybord DOM element id
 */
com.rehabstudio.googlebooth.ui.Keyboard.prototype.initialise = function(name)
{
	var that = this;
	
	/**
	* String - Reference to the keyboard DOM element id
	* @type {String}
	*/
	this.name = name;

	/**
	* com.rehabstudio.Event - Instance of Observer used to notify keyboard events to attached listeners
	* @type {com.rehabstudio.Event}
	*/
	this.keyboardEvent = new com.rehabstudio.Event(this);
	
	/**
	* com.rehabstudio.Event - Instance of Observer used to notify keyboard events to attached listeners
	* @type {Object}
	*/
	this.keys = $j('#'+name + ' .key');

	/**
	* Bool - Flag, indicates if the keyboard has been currently set to operate in uppercase mode
	* @type {Bool}
	*/
	this.uppercase = false;
	this.keys.mousedown(function()
	{
		var thekey = this;
		that.typekey(thekey.getAttribute('data-input'));
		thekey.setAttribute('class','key letter keyhit');
		
		var timeOutHit = setTimeout(function()
		{
			thekey.setAttribute('class','key letter');
		},100);

	});

}

/**
 * Switches the keyboard between upper and lower case mode alternatively
 */
com.rehabstudio.googlebooth.ui.Keyboard.prototype.keysToUppercase = function()
{
	var self = this;
	this.uppercase = this.uppercase?false:true;
	
	$j('.letter').each(function(index)
	{
		var newVal = self.uppercase?this.innerHTML.toUpperCase():this.innerHTML.toLowerCase();
		this.innerHTML = '<span>' + newVal + '</span>';
	});

}

/**
 * Type Function
 */
com.rehabstudio.googlebooth.ui.Keyboard.prototype.typekey = function(val)
{
	val = val.toLowerCase();
	switch(val)
	{
		case '_shift':
			this.keysToUppercase();
		break;

		case '_send':
			this.keyboardEvent.notify({"message": "submit_button_click", "data":null});
		break;

		case '_canc':
			this.keyboardEvent.notify({"message": "key_stroke_canc", "data":null});
		break;

		default:
			this.keyboardEvent.notify({"message": "key_stroke", "data":this.uppercase?val.toUpperCase():val});
		

	}
	
}

/**
 * Attach Function, wrapper for Event::attach()
 */
com.rehabstudio.googlebooth.ui.Keyboard.prototype.attach = function(val)
{
	this.keyboardEvent.attach(val);
}

/**
 * Attach Function, wrapper for Event::attach()
 */
com.rehabstudio.googlebooth.ui.Keyboard.prototype.attach_bind = function(val,ref)
{
	this.keyboardEvent.attach_bind(val,ref);
}