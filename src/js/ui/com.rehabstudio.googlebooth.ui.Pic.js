/**
 * @constructor
 * @param {Object} container The flash container dom element
 */
com.rehabstudio.googlebooth.ui.Pic = function(holder,imageVo)
{



	this._holder = holder;
	this.imageId = imageVo.id;
	this._vo = imageVo;

	var _self = this;


	this.tweennew = null;
	this.tweenside = null;
	this.tweenexit = null;
	this.tweenkill = null;
	this.position = {alpha:50, w:747, h:420, x: -393, y: -230, rotation: 0, bw:20};

	this._holder.append("<div id=\"image_"+this._vo.id+"\" class=\"pic\"></div>");
	this._target = document.getElementById("image_"+this._vo.id);
	this._imageHolder = this._holder.find("#image_"+this._vo.id);


	this._closeButton = document.createElement('div');
	this._imageHolder.append(this._closeButton);
	this._closeButton.setAttribute("class", "close blue-btn");
	this._ico = document.createElement('span');
	this._ico.setAttribute("class", "ico");
	this._closeButton.appendChild(this._ico);
	this._imageHolder.find(".close").fadeOut(0);

	var image = new Image();
	image.src = this._vo.getLowResData();
	this._imageHolder.append(image);

	//this.objPosition = {"x":0, "y":0, "rotation":0, "width":690, "height":460};


	//this.objPosition = {obj: this._imageHolder, x : this._imageHolder.style.marginLeft};



	/**
	* Dispatched on Pic Events along with a message
	* @type {com.rehabstudio.Event}
	*/
	this.picEvent = new com.rehabstudio.Event( this );


	
}


/**
 *  Kill
 */
com.rehabstudio.googlebooth.ui.Pic.prototype.kill = function()
{
	var self = this;

	if(this.tweenshow) this.tweenshow.stop();
	if(this.tweenside) this.tweenside.stop();
	if(this.tweenexit) this.tweenexit.stop();


	this.tweenshow = new TWEEN.Tween(self.position)
	.to({alpha: 0}, 1000)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		self._imageHolder.css("opacity",(self.position.alpha/100));
	})
	.onComplete(function()
	{
		self._imageHolder.remove();
	})
	.start();

	this._imageHolder.find(".close").fadeOut();
	
}

/**
 *  Set Button Visibility
 */
com.rehabstudio.googlebooth.ui.Pic.prototype.enableRemoveAction = function()
{
	var self = this;
	
	var obj = this._imageHolder.find(".close");
	obj.css('pointer-events', 'all');
	obj.stop().fadeIn();

	this._closeButton.addEventListener('mousedown', function()
	{
		self.picEvent.notify({"message": "select_image_to_remove", "pic":self});

	});
}
/**
 *  Set Button Visibility
 */
com.rehabstudio.googlebooth.ui.Pic.prototype.disableRemoveAction = function()
{
	
	var obj = this._imageHolder.find(".close");
	
	obj.css('pointer-events', 'none');
	obj.stop().fadeOut();

	this._closeButton.removeEventListener('click');
}


/**
 * Animate Position
 */
com.rehabstudio.googlebooth.ui.Pic.prototype.moveToSide = function(xPos,yPos, delay, showclose)
{
	
	var self = this;
	var showclose = showclose;

	var randRot = randomXToY(-20,20);

	if(this.tweenside) this.tweenside.stop();

	this.tweenshow = new TWEEN.Tween(self.position)
	.to({w:180, h:101, x: xPos, y: yPos, rotation: randRot, bw:5}, 1000)
	.delay(delay)
	.easing(TWEEN.Easing.Exponential.InOut)
	.onUpdate(function()
	{
		self._imageHolder.css("width",self.position.w+"px");
		self._imageHolder.css("height",self.position.h+"px");
		self._imageHolder.css("marginLeft",self.position.x+"px");
		self._imageHolder.css("marginTop",self.position.y+"px");
		self._imageHolder.css("borderWidth",self.position.bw+"px");
		self._target.style.webkitTransform = 'rotate(' + self.position.rotation + 'deg)';

	})
	.onComplete(function()
	{
		self.enableRemoveAction();
	})
	.start();

}


/**
 * Animate Position
 */
com.rehabstudio.googlebooth.ui.Pic.prototype.moveToPosition = function(xPos,yPos, delay, showclose)
{
	
	var self = this;

	var showclose = showclose;


	this.disableRemoveAction();

	var img = this._imageHolder;
	if(this.tweenexit) this.tweenexit.stop();
	if(this.tweenshow) this.tweenshow.stop();
	
	if(xPos+"px" === img.css("marginLeft")) return;

	
	var randRot = randomXToY(-20,20);


	this.tweenshow.stop();
	this.tweenside = new TWEEN.Tween(self.position)
	.to({x: xPos, y: yPos, rotation: randRot}, 1000)
	.delay(delay)
	.easing(TWEEN.Easing.Exponential.InOut)
	.onUpdate(function()
	{
		self._imageHolder.css("marginLeft",self.position.x+"px");
		self._imageHolder.css("marginTop",self.position.y+"px");
		self._target.style.webkitTransform = 'rotate(' + self.position.rotation + 'deg)';

	}).start();


}




/**
 * Animate Position
 */
com.rehabstudio.googlebooth.ui.Pic.prototype.moveOutside = function(delay)
{
	
	var self = this;
	var img = this._imageHolder;
	
	this.disableRemoveAction();

	if(this.tweenshow) this.tweenshow.stop();
	if(this.tweenside) this.tweenside.stop();

	this.tweenexit = new TWEEN.Tween(self.position)
	.to({x: 2000}, 1500)
	.delay(delay)
	.easing(TWEEN.Easing.Exponential.InOut)
	.onUpdate(function()
	{
		self._imageHolder.css("marginLeft",self.position.x+"px");
	}).start();

}

