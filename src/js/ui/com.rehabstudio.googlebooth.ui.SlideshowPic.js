/**
 * @constructor
 * @param {Object} container The flash container dom element
 */
com.rehabstudio.googlebooth.ui.SlideshowPic = function(holder,imageSrc, imgId, side, small)
{



	this._holder = holder;
	this.src = imageSrc;
	this.imgId = imgId;
	
	this.side = side;
	this.small = small;

	this.dead = true;

	this.tweenshow = null;
	this.tweendie = null;
	this.position = {alpha:0, x: 0, y: 1500, rotation: 0};


	this._holder.append("<div id=\""+this.imgId+"\" class=\"slide "+side+" "+small+"\"></div>");
	this._target = document.getElementById(this.imgId);
	this._imageHolder = this._holder.find("#"+this.imgId);

	//var image = new Image();
	//image.src = this.src;
	//this._imageHolder.append(image);
	
	this._imageHolder.append(this.src);

	if(small)
	{
		this.wid = randomXToY(150, 200);
	}
	else
	{
		this.wid = randomXToY(350, 400);
	}

	this.position.y = 1500;

	if(this.side == "right") this.position.y = -1500;


	this._imageHolder.css("width", this.wid+"px");

}


/**
 *  Kill
 */
com.rehabstudio.googlebooth.ui.SlideshowPic.prototype.kill = function()
{
	var self = this;

	this.tweenshow.stop();

	this.tweendie = new TWEEN.Tween(this.position)
	.to({alpha:0}, 600)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		self._imageHolder.css("opacity",(self.position.alpha/100));
	})
	.onComplete(function ()
	{
		self._imageHolder.remove();
	});

	this.tweendie.start();


}


/**
 * Animate Position
 */
com.rehabstudio.googlebooth.ui.SlideshowPic.prototype.moveToPosition = function(xPos,yPos, delay, waitbeforekill)
{
	
	var self = this;

	var showclose = showclose;

	var img = self._imageHolder;

	this.dead = false;

	var randRot = randomXToY(-40,40);

	randRot += 360;
	
	if(this.small)
	{
		if(yPos<-50)
		{
			yPos+=this.hei;
		}
		
	}


	this.tweenshow = new TWEEN.Tween(self.position)
	.to({alpha:100, x: xPos, y: yPos, rotation: randRot}, 2000)
	.delay(0)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		self._imageHolder.css("opacity",(self.position.alpha/100));
		self._imageHolder.css("marginLeft",self.position.x+"px");
		self._imageHolder.css("marginTop",self.position.y+"px");
		self._target.style.webkitTransform = 'rotate(' + self.position.rotation + 'deg)';

	}).start();

}

