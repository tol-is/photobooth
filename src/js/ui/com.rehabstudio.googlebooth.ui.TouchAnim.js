/**
 * @constructor
 * @param {Object} container The flash container dom element
 */
com.rehabstudio.googlebooth.ui.TouchAnim = function(container)
{

	this.container = container;
	this.position = 0;
	this.steps = 16;
	this.timer = null;

	this.play();

}


/**
 * TouchAnim Play
 */
com.rehabstudio.googlebooth.ui.TouchAnim.prototype.play = function()
{
	var self = this;
	
	if(this.timer) clearTimeout(this.timer);

	var moveBackground = function()
	{
		self.position += 1;

		if(self.position === this.steps)
		{
			self.position = 0;
			return;
		}

		self.container.css("backgroundPosition", 81-(self.position*81)+"px");

		self.timer = setTimeout(moveBackground,40);

	};

	moveBackground();

}