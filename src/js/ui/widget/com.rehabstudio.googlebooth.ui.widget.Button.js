/*
 * @constructor
 */
com.rehabstudio.googlebooth.ui.widget.Button = function()
{
	//
}

/**
 * @function initialise
 * 
 * @param name    - references object's DOM element id
 * @param valueIn - sets value attribute
 */

com.rehabstudio.googlebooth.ui.widget.Button.prototype.initialise = function(name,valueIn)
{
	//Protected
	this.__name = name;
	this.__valueIn = valueIn;
	this._callback = null;
	
	this._jqButton = $j('#' + this.__name);
	this._domButton = document.getElementById(this.__name);

	if(valueIn !== undefined)
	{
		this._value = valueIn;
	}
	else
	{
		this._value = '';
	}
		
	this.setValue(this._value,true);
}

/* ------------------------------------------- API ------------------------------------------- */

com.rehabstudio.googlebooth.ui.widget.Button.prototype.setCallback = function(func)
{
	this._callback = func;
	this._jqButton.click(this._callback);
}

com.rehabstudio.googlebooth.ui.widget.Button.prototype.setValue = function(strValue,reset)
{
	if(reset !== undefined && reset)
	{
		this._value = strValue;
	}
	else
	{
		this._value += strValue;
	}
	this._domTextInput.setAttribute('value',this._value);
}