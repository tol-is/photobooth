/**
 * @constructor
 * @param {Object} container The keyboard container dom element
 */
com.rehabstudio.googlebooth.ui.widget.TextInput = function()
{
	//
}

com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.initialise = function(name,valueIn,delegate)
{
	var self = this;
	
	this.__name = name;
	this.__valueIn = valueIn || '';
	if(delegate !== undefined && delegate !== false){
		this.setDisplayDelegate(delegate);
	}
		
	// @about Protected property (flag), indicates if we should (true) or not (false) remove ( = clear) the input existing value upon adding a new value. If not we add new to existing
	this.__clearState = true;
	this._validationRule = {};

	this.welcomeString = valueIn?valueIn:'';

	this._value = '';
	this.__intValidator = {
			init:function(objRule,domMsgTarget)
			{
				this._objRule = objRule || false;
				this._domMsgTarget = domMsgTarget|| false;
				this.controlValidationFeedback = objRule.controlFeedback;
			},
			validate:function(inputValue)
			{
				var inputVal = inputValue;
				var boolResult = true;
				var regexRule = this._objRule.rule;
				if(this.controlValidationFeedback)this._domMsgTarget.innerHTML = '';
				
				if(regexRule.test(inputVal))
				{
					//ok
				} 
				else
				{
					this._showMessage(this._objRule.msg);
					boolResult = false;
				}
				return boolResult;
			},
			_showMessage:function(msg)
			{
				if(this._domMsgTarget)
				{
					if(this.controlValidationFeedback)this._domMsgTarget.innerHTML  +=  msg;
						this._domMsgTarget.style.visibility = 'visible';
				}
				else
				{
					Logger.debug('EmailInput validator :: invalid dom target or generic error');
				}
			}
	};
	
	this.__domTextInput = document.getElementById(this.__name);

	if(this.__valueIn !== undefined)
	{
		this.initValue(this.__valueIn);
	}
	else
	{
		this.__valueIn = '';
	}
	this._validationRule = {};
};

/* ------------------------------------------- API ------------------------------------------- */

com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.getValue = function()
{
	return this.__domTextInput.getAttribute('value');
};

com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.initValue = function(strValue)
{
	this.__domTextInput.setAttribute('value',strValue);
	if(this.updateDisplayDelegate !== undefined)
		this.updateDisplayDelegate();
};

com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.setValue = function(strValue)
{
	if(this.__clearState)
	{
		this._value = strValue;
	}
	else
	{
		this._value += strValue;
	}

	this.__clearState = false;
	this.__domTextInput.setAttribute('value',this._value);

	if(this.updateDisplayDelegate !== undefined)
		this.updateDisplayDelegate();
}

com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.updateDisplayDelegate = function()
{
	//By Default do nothing, leave for sub classes to implement what to do with it, i.e. how to update the Delegate display
}

com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.setDisplayDelegate = function(elem)
{
	this.displayDelagate = elem;
}

com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.popValue = function(strValue)
{
	if(this.getValue() == this.__valueIn)
	{
		this.setValue('',true);
	}
	else if(this._value.length > 0)
	{
		this._value = this._value.substring(0,this._value.length-1);
		this.__domTextInput.setAttribute('value',this._value);
	}

	if(this.updateDisplayDelegate !== undefined)
		this.updateDisplayDelegate();
}


com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.setValidationRule = function(objValidationRule)
{
	this._validationRule = objValidationRule;
}

com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.setValidationTarget = function(domValidationTarget)
{
	this._validationTarget = domValidationTarget;
}


com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.clearValidationTarget = function()
{
	if(this.__intValidator.controlValidationFeedback)this._validationTarget.innerHTML = '';
	this._validationTarget.style.visibility = 'hidden';
}

com.rehabstudio.googlebooth.ui.widget.TextInput.prototype.runValidation = function()
{
	// @about In general we want the input value to stay as is, also after validation
	this.__clearState = false;
	// @about But if the value still is the initial greeting, then we want to clear it
	if(this.getValue().indexOf(this.__valueIn) === 0)
	{
		this.__clearState = true;
	}
	this.__intValidator.init(this._validationRule,this._validationTarget);
	return(this.__intValidator.validate(this.getValue()));
}
