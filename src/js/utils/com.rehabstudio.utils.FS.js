/**
* Provides methods for common File System operations by scripting tyhe Chrome File System API
* Note: All operations are engineered ajax-style, i.e. success / fail callbacks need to be embedded within the method call which takes care of starting the process
* and further processes are also embedded in the same fashion
*
* @constructor
*/
com.rehabstudio.utils.FS = function()
{

  /**
  * Instance of the actual file system (low level API)
  * @type {Object}
  */
	this.fs = null;
  
  /**
  * Dispatched when the request is complete
  * @type {com.rehabstudio.Event}
  */
  this.FSbaseEvent  = new com.rehabstudio.Event(this);
  
  var self = this;
  window.webkitStorageInfo.requestQuota(PERSISTENT, 100*1024*1024, function(grantedBytes) {
        window.webkitRequestFileSystem(PERSISTENT, grantedBytes, onInitFS, function(error){self.errorHandler(error,self)});
        }, function(e) {
            self.FSbaseEvent.notify({"message": "fs_error", "data": e});
  });

  function onInitFS(fs){
        self.fs = fs.root;
        self.FSbaseEvent.notify({"message": "fs_init", "data": "success"});
  }
}

/**
* Initialise ther FS object
*
*/
com.rehabstudio.utils.FS.prototype.initialise = function()
{
	//
}

/**
* Saves a file to the local File System , async notification on result
*
* @param {String} filePath    As in '/directory' - an absolute path to the file directory
* @param {String} fileTitle   The file title, as in 'title.jpg'
* @param {String} fileData    The file contents
* @param {Bool}   softWrite   If true, the FS will only throw an error upon finding the file exists already, otherwise it will try to overwrite
* @param {String} initiator   A 'token' from the object that started (requested) the operation. This will be re-forwarded in the final notification
*/
com.rehabstudio.utils.FS.prototype.saveFile = function(filePath,fileTitle,fileData,softWrite,initiator) {
    var objCreateOptions ={};

    if(softWrite === undefined || !softWrite){ 
      objCreateOptions = {create: true};
    }
	  else {
      objCreateOptions = {create: true, exclusive:true};
    }

    var that = this;
    var savePath = filePath + '/' + fileTitle;
    Logger.debug('FS Save ++++ begin saveFile routine: path is ' + savePath);
    this.fs.getFile(
            savePath, 
            objCreateOptions,
            function(fileEntry) {
                Logger.debug('FS Save ++++ getFile - inside callback');
                fileEntry.createWriter(
                        function(fileWriter) {
                            fileWriter.onwriteend = function(e) {
                                Logger.debug('Write completed.');
                                that.FSbaseEvent.notify(
                                     {'filePath':savePath,'message':'file_save_success','initiator':initiator});
                            };
                            fileWriter.onerror = function(e) {
                                Logger.debug('Write failed: ' + e.toString());
                            };
                            var webKitBlobBuilder = new window.WebKitBlobBuilder();
                            webKitBlobBuilder.append(fileData);
                            fileWriter.write(webKitBlobBuilder.getBlob('application/octet-stream'));
                        }, 
                        function(error){
                        	that.errorHandler(error,that)
                        }
                );
            }, function(error){
            	that.errorHandler(error,that)
            }
    );
}

/**
* Adds the passed in content to the passed in file, async notification on result
*
* @param {String} fileFullPath The absolute path to the file to append to
* @param {String} appendContent The content to append
* @param {String} initiator A 'token' from the object that started (requested) the operation. This will be re-forwarded in the final notification
*/
com.rehabstudio.utils.FS.prototype.appendToFile = function(fileFullPath,appendContent,initiator) {
    var self = this;
    this.fs.getFile(fileFullPath, {create: false}, function(fileEntry) {
    	fileEntry.createWriter(
    			function(fileWriter) {
        			fileWriter.seek(fileWriter.length); // Start write position at EOF.
			        var webKitBlobBuilder = new window.WebKitBlobBuilder();
			        webKitBlobBuilder.append(appendContent);
			        fileWriter.write(webKitBlobBuilder.getBlob('text/plain'));
			        Logger.debug('Content written to file - ok :: ' + appendContent);
			        self.FSbaseEvent.notify({"message": "file_write_complete", "responseData": appendContent,'initiator':initiator});
    			}, 
    			function(error){
    				self.errorHandler(error,self);
    			});
  		}, 
  	function(error){
  		self.errorHandler(error,self);
  	});
}

/**
* Reads the contents of a file and notifies them when done. Async process between call and notifiaction
*
* @param {String} fileFullPath Lorem ipso dolor sita amet
* @param {String} initiator A 'token' from the object that started (requested) the operation. This will be re-forwarded in the final notification
*/
com.rehabstudio.utils.FS.prototype.readFile = function(fileFullPath,initiator) {
    Logger.debug('Read file :: start. initiator is ' + initiator + ' - looking for file ' + fileFullPath);
    var self = this;
    this.fs.getFile(fileFullPath, {create:false}, function(fileEntry) {
    fileEntry.file(function(file) {
       var reader = new FileReader();
       reader.onloadend = function(e) {
         self.FSbaseEvent.notify({"message": "file_read_complete", "responseData": this.result,'initiator':initiator,'fullPath':fileFullPath});
         Logger.debug('FS read file :: complete. initiator was ' + initiator);
       };
       reader.readAsText(file);
    }, function(error){
    	self.errorHandler(error,self)
    });

  }, function(error){
  	self.errorHandler(error,self)
  });
}

/**
* Creates a folder. Note: the path leading to the folder must already exist (At the beginning only '/' exists).
* This method is not recursive so if asking to create '/folder1/folder2' folder1 will need to be already there or the FS will throw an error and exit
*
* @param {String} dirName The folder to create, absolute path (parent must exist)
* @param {String} initiator A 'token' from the object that started (requested) the operation. This will be re-forwarded in the final notification
*/
com.rehabstudio.utils.FS.prototype.createFolder = function(dirName,initiator) {
    var  that = this;
    Logger.debug('Fs._createUserFolder for ' + dirName + ' inititator is ' + initiator);
    this.fs.getDirectory(
            dirName, 
            {create: true}, 
            function(dirEntry) {
                Logger.debug('Folder create successful');
                that.FSbaseEvent.notify({'data':dirName,'message':'folder_created','initiator':initiator});}, 
            function(e){
                Logger.debug('Folder create failed');
            }
    );  
}

/**
* Reads the contents of a folder and notifies them as separate arrays for files (arrFiles) and subfolders (arrDirectories)
*
* @param {String} dirName The folder whose content are to be read, absolute path
* @param {String} initiator A 'token' from the object that started (requested) the operation. This will be re-forwarded in the final notification
*/
com.rehabstudio.utils.FS.prototype.folderContent = function(dirName,initiator) {
  Logger.debug('Start: Fs->folderContent for directory ' + dirName + ' - initiator is ' + initiator);
  var arrTmpFiles = [];
  var arrTmpDirs = [];
  var that = this;
  this.fs.getDirectory(
      dirName,
      {create:false},
      function(dirEntry){
        Logger.debug('Fs->folderContent :: we are in  folder ' + dirName + ' - initiator is ' + initiator);
        var dirReader = dirEntry.createReader();
        var entries = [];
        var readEntries = function() {
          dirReader.readEntries (function(results) {
            Logger.debug('Fs->folderContent --  dir ' + dirName + ' results length : ' + results.length + ' - initiator is ' + initiator);
            if (!results.length) {
              //do nothing, array will be returned empty ...
            } else { 
              for(var i = 0;i < results.length; i++){
                var fileEntry = results[i];
                if(fileEntry.isFile){
                  arrTmpFiles.push(
                      {
                        name:fileEntry.name,
                        path:fileEntry.fullPath,
                        locUrl:fileEntry.toUrl
                      }
                  );
                  Logger.debug('Fs->folderContent --  added file:: ' + fileEntry.fullPath);
                } else if(fileEntry.isDirectory){
                  arrTmpDirs.push(fileEntry.name);
                  Logger.debug('Fs->folderContent --  added directory:: ' + fileEntry.fullPath);
                }
              }
              Logger.debug('Fs->folderContent :: finished');
            }
            that.FSbaseEvent.notify({'responseData':{'arrFiles':arrTmpFiles,'arrDirectories':arrTmpDirs,'dirName':dirName},'message':'folder_content_parsed','initiator':initiator});
          }, function(error){
          	that.errorHandler(error,that)
          });
        }
        readEntries();
      },
     function(error){that.errorHandler(error,that)}
  );
}

/**
* Deletes a folder
* 
* @param {String} dirPath Absolute path to the folder to delete
* @param {String} initiator A 'token' from the object that started (requested) the operation. This will be re-forwarded in the final notification
*/
com.rehabstudio.utils.FS.prototype.deleteFolder = function(dirPath,initiator) {
    Logger.debug('Directory delete : ' + dirPath);
    var  that = this;
    this.fs.getDirectory(dirPath, {}, function(dirEntry) {
	    dirEntry.removeRecursively(function() {
	      Logger.debug('FS :: Directory removed successfully : ' + dirPath);
	      that.FSbaseEvent.notify({'data':dirPath,'message':'directory_delete_success','initiator':initiator}); 
	    }, function(error){
	      that.errorHandler(error,that);
	      that.FSbaseEvent.notify({'data':dirPath,'message':'directory_delete_failed'}); 
	    });

  }, function(error){that.errorHandler(error,that)});
}

/**
* Deletes a folder
*
* @param {String} fullPath Absolute path to the file to delete
* @param {String} initiator A 'token' from the object that started (requested) the operation. This will be re-forwarded in the final notification
*/
com.rehabstudio.utils.FS.prototype.deleteFile = function(fullPath,initiator) {
    var that = this;
    this.fs.getFile(fullPath, {create: false}, function(fileEntry) {
    fileEntry.remove(function() {
      Logger.debug('File removed : ' + fullPath);
      that.FSbaseEvent.notify({'msg':'file_deleted','data':fullPath,'initiator':initiator});
    }, function(error){
    	that.errorHandler(error,that);
    });

    }, function(error){
    	that.errorHandler(error,that);
    });
}

/**
* Generic error handlers for failed File System operations
*
* @param {Object} error An object containing error parameters
* @param {Object} ref For closure purposes, reference to the object the error handler is attached to
*/
com.rehabstudio.utils.FS.prototype.errorHandler = function(error,ref){
            var msg = "";
            var self = ref;
            switch (error.code) {
                case FileError.QUOTA_EXCEEDED_ERR:
                    msg = 'QUOTA_EXCEEDED_ERR';
                    break;
                case FileError.NOT_FOUND_ERR:
                    msg = 'NOT_FOUND_ERR';
                    break;
                case FileError.SECURITY_ERR:
                    msg = 'SECURITY_ERR';
                    break;
                case FileError.INVALID_MODIFICATION_ERR:
                    msg = 'INVALID_MODIFICATION_ERR';
                    break;
                case FileError.INVALID_STATE_ERR:
                    msg = 'INVALID_STATE_ERR';
                    break;
                case 'savedata_useremail_error':
                    msg = 'Error in setting current user email before save data';
                    break;
                default:
                    msg = 'Unknown Error';
            }
            
            self.FSbaseEvent.notify({"message": "fs_error", "data": msg});
            Logger.debug('ERROR fs_error notified :: ' + msg);
}