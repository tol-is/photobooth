/**
 * Covers all logs file system related operations. 
 * Instance of this calss is used within the File System Service, which acts as an intermediate between the Log and the rest of the application (Model)
 *
 * @constructor
 * @param {com.rehabstudio.utils.FS} fs Reference to an instance of the file system object 
 */
com.rehabstudio.utils.Log = function(fs)
{
    /**
    * Reference to the file system object
    * @type {com.rehabstudio.utils.FS}
    */
	this.fs = fs;
}

/**
 * Initialises the Log obj for operation. Sets up the tokens property which stores a vo of 'globals' to tag (tokenise) processes - they are embedded in calls to the File System
 * operation and will be then forwarded by the FIle System itself to identify the owner of each file system operation (due to the async nature of the process)
 *
 */
com.rehabstudio.utils.Log.prototype.initialise = function()
{
	var self = this;

	this.logs = null;
    this.logFilename = 'log.txt';
    this.logNewLine = '_END_';
    this.tokens = {
    	logsread:'LOG_READ',
    	logswrite:'LOG_WRITE',
    	logscreate:'LOG_SET'
    }
}

/**
*  Creates a log file via the saveFile File System method, which it invokes asking a soft write: this will gracefully fail on finding the log file exists already (an error will be thrown) thus avoiding to overwrite the log file
*  Passes a token for notification (NB 'token' refers to the User's tokens{} property, not the datetime used to identify users submissions) 
*
*/
com.rehabstudio.utils.Log.prototype.createLogsFile = function() {
	var logInitString = '#Logs start - ' + new Date() + this.logNewLine;
	this.fs.saveFile('',this.logFilename,logInitString,true,this.tokens.logscreate)
}

/**
*  Adds an entry in the log file, using the text passed in and addin a new line string (property); passes a token for File System notification (NB 'token' refers to the User's tokens{} property, not the datetime used to identify users submissions)
*
*  @param {String} logEntryText The text to be added - Log object does not know about result type
*/
com.rehabstudio.utils.Log.prototype.logEntry = function(logEntryText) {
    this.fs.appendToFile(this.logpath(),logEntryText + this.logNewLine,this.tokens.logswrite);
    this.refreshLogs();
}

/**
*  Reads the logs; uses the FS readFile and passes a token for notification (NB 'token' refers to the User's tokens{} property, not the datetime used to identify users submissions)
*
*/
com.rehabstudio.utils.Log.prototype.readLogs = function() {
    this.fs.readFile(this.logpath(),this.tokens.logsread);
}

/**
*  Calls the local readLogs method
*
*/
com.rehabstudio.utils.Log.prototype.refreshLogs = function() {
	this.readLogs();
}

/**
*  Returns the absolute path to the log file
*
*/
com.rehabstudio.utils.Log.prototype.logpath = function() 
{ 
      return '/' + this.logFilename;
}