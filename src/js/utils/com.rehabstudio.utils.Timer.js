
/**
 * Timer Utility
 * @constructor
 */
com.rehabstudio.utils.Timer = function(id, cycle) {
	
	this._id = id;
	
	/*
	 * @private
	 * @type {number}
	 */
	this._timer;


	this.tickCycle = cycle;
    this.countCycle = cycle;
   

	/**
	 * @private
	 * @type {number}
	 */
	 this._interval = 1000;


	 this.timerEvent = new com.rehabstudio.Event( this );

}

/**
 * Starts the timer running
 */
com.rehabstudio.utils.Timer.prototype.startTimer = function() {
	var self = this;
	//this.forceTick();
	clearTimeout(this._timer);
	this._timer = setTimeout(function(){
		self.tick();
	},  this._interval);
} 


/**
 * Stops the current time from running
 */
com.rehabstudio.utils.Timer.prototype.resetTimer = function() {
	this.countCycle = this.tickCycle;
}



/**
 * Stops the current time from running
 */
com.rehabstudio.utils.Timer.prototype.stopTimer = function() {
	 //clearTimeout(this._timer);
	 this.countCycle = -1;
}


/**
 * Tick function
 */
com.rehabstudio.utils.Timer.prototype.forceTick = function() {
	this.countCycle = this.tickCycle;
	Logger.debug("tick");
    this.timerEvent.notify({"message": "tick", "source":this._id});
}

/**
 * Set Tick Cycle function
 */
com.rehabstudio.utils.Timer.prototype.setCycle = function(value) {
	this.tickCycle = value;
    this.countCycle = this.tickCycle;
}


/**
 * Tick function
 */
com.rehabstudio.utils.Timer.prototype.tick = function() {


	var self = this;
	if(this._id == "standByTimer"){
		Logger.debug(this.countCycle);	
	}
	
	if ( this.countCycle == 0) {
     	this.countCycle = this.tickCycle;
     	this.timerEvent.notify({"message": "tick", "source":this._id});
    } else {
        this.countCycle--;
    }

    //it its stopped stop cycle
    if(this.countCycle>=0){
    	clearTimeout(this._timer);
    	this._timer = setTimeout(function(){
			self.tick();
		},  this._interval);
	}

}	