/**
* Translation class, takes care of re-applying translated text (a translation set) after language selection
*
* @constructor
* @param {Object} translation A value Object (translation set)containing all application text paragraphs as members and their value
*/
com.rehabstudio.utils.Translator = function(translation, rtl)
{
	this.translation = translation;
	this.isrtl = rtl;




	this.strTCDelimiterStart = '_TCSTART_';
	this.strTCDelimiterEnd = '_TCEND_';
}

/**
* Takes care of applying the translation object to the target it is asked to translate
*
* @param item    {String} The translation item
* @param target  {Object} Either a DOM reference or a JQuery set
* @param options {Object} A value object containing options for the translation - see following template {'jquery':false[default]|true,'makebold':false[default]|true,'makeitalic':false[default]|true,'useAttribute':false[default]|attribute to use(i.e. 'value')}
*/
com.rehabstudio.utils.Translator.prototype.translate = function(item,target,options)
{
	if(options === undefined)options = {};
	// @about Setup defaults
	if(options.jquery === undefined)options.jquery = false;
	if(options.useAttribute === undefined)options.useAttribute = false;
	if(options.returnVal === undefined)options.returnVal = false;

	// @about If required, just return the translated value
	if(options.returnVal) {
		return this.translation[item];
	}
	// @about Otherwise, take care of applyi§ng the translation
	if(options.useAttribute){
		if(!options.jquery){
			if(target)target.setAttribute(options.useAttribute,this.translation[item]);
		}
		else {
			if(target)target.attr(options.useAttribute,this.translation[item])
		}
	} else {
		if(!options.jquery){
			if(target){
				target.innerHTML = options.formatter ? this[options.formatter](this.translation[item]) : this.translation[item];

				if(this.isrtl){
					target.setAttribute("dir", "rtl");
				}else{
					target.removeAttribute("dir");
				}
			}
		}
		else {
			if(target){

				target.html(options.formatter ? this[options.formatter](this.translation[item]) : this.translation[item]);

				if(this.isrtl){
					target.addClass("rtl");
				}else{
					target.removeClass("rtl");
				}
			}
		}
	}
}

/**
* Helper, udsed to format the Terms and Conditions linked copy (line)
*
* @param {String} rawString The raw string passed from the configuration
*/
com.rehabstudio.utils.Translator.prototype.formatTCManage = function(rawString)
{
     var formattedString = rawString.substring(0,rawString.indexOf(this.strTCDelimiterStart)) +
     "<a href='#' id='termsandconditions-modal' class='tc-manage'>" +
     rawString.substring(rawString.indexOf(this.strTCDelimiterStart) + this.strTCDelimiterStart.length,rawString.indexOf(this.strTCDelimiterEnd)) +
     "</a>" +
     rawString.substring(rawString.indexOf(this.strTCDelimiterEnd) + this.strTCDelimiterEnd.length,rawString.length);
     return formattedString;
}