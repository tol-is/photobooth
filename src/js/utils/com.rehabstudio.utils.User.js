/**
 * Covers all user (user images) file system related operations. 
 * Instance of this class is used within the File System Service, which acts as an intermediate between the User and the rest of the application (Model)
 *
 * @constructor
 * @param {com.rehabstudio.utils.FS} fs Reference to an instance of the file system object
 */

com.rehabstudio.utils.User = function(fs)
{
  /**
  * Reference to the file system object
  * @type {com.rehabstudio.utils.FS}
  */
	this.fs = fs;
  /**
  * Current user data stored (waiting to be actioned)
  * @type {Object}
  */
  this.data = null;
  /**
  * The current user
  * @type {String}
  */
  this.user = null;
  /**
  * Array of cued Images, packed in objects with process identifiers for laer retrieval
  * @type {Array}
  */
  this.cue = null;
  /**
  *  Stores a vo of 'globals' to tag (tokenise) processes - they are embedded in calls to the File System operation and will be then forwarded by the FIle System itself to identify the owner of each file system operation (due to the async nature of the process)
  * @type {Object}
  */
  this.tokens = null;
}

/**
 * Initialises the User obj for operation. Sets up the tokens property which
 *
 */
com.rehabstudio.utils.User.prototype.initialise = function()
{
	var self = this;

	this.data = {};
  this.cue = [];
  this.usersFolders = [];
  
  this.tokens = {
  	userimageread:'USER_IMAGE_READ',
  	usersfolderslist:'USERS_LIST',
    usersfolderslistcron:'USERS_LIST_CRON',
  	userimageslist:'USERIMGS_LIST',
    userimagesave:'USER_IMAGE_SAVE',
    userfoldercreate:'USER_FOLDER_CREATE'
  }
}

/**
 * Getter - returns the current user (email and datetime user+submission identifier)
 *
 */

com.rehabstudio.utils.User.prototype.getUser = function() 
{
      return this.user;
};

/**
*  Save ImageVO objects to File System after creating the folder for them (based on the incoming data object)
*  To make sure the user creation process and the image save process do not overlap, it asks the File System to create the folder and cues up the remaining parameters
*  to be saved as a callback - a token is stored with the image datra in the cue which will enable the File Sys service to invoke the cue processing adfter the folder has been created
*
*  @param {Object} data COntains the user email + submission token and ImageVO data as in {imageData:[],userEmailAndToken:''}
*/
com.rehabstudio.utils.User.prototype.saveUserImageData = function(data)
{
  var self = this;
  this.setData(data);
  for(var i = 0; i< this.data.imageData.length;i++){
          var imageVO = this.data.imageData[i];
          var imageData = imageVO.imageData;
          var imageTitle = imageVO.id;
          // @about add image to the cue - when the corresponding user's directory is created the Service will retrieve it and run it for save
          this.cue.push([
            this.tokens.userfoldercreate,
            this.data.userEmailAndToken,
            imageTitle,
            imageData
          ])
  }
  this.initUser(this.data.userEmailAndToken);
}

/**
*  Reads a user image given the path; uses the FS readFile and passes a token for notification (NB 'token' refers to the User's tokens{} property, not the datetime used to identify users submissions)
*
*  @param {String} fullPath Image full path
*/
com.rehabstudio.utils.User.prototype.readUserImage = function(fullPath)
{
	this.fs.readFile(fullPath,this.tokens.userimageread);
}

/**
*  Deletes a user image given the path; uses the FS deleteFile and passes a token for notification (NB 'token' refers to the User's tokens{} property, not the datetime used to identify users submissions)
*
*  @param {String} fullImagePath The Image full path
*/
com.rehabstudio.utils.User.prototype.deleteUserImage = function(fullImagePath)
{
	this.fs.deleteFile(fullImagePath,this.tokens.userimagedelete)
}

/**
* Deletes a user directory given the path; uses the FS deleteFolder and passes a token for notification (NB 'token' refers to the User's tokens{} property, not the datetime used to identify users submissions)
*
*  @param {String} dirPath
*/
com.rehabstudio.utils.User.prototype.deleteUserDirectory = function(dirPath)
{
	this.fs.deleteFolder(dirPath,this.tokens.userfolderdelete);
}

/**
*  Uses the FS folderContent functionality to retrieve a list of images from a given user folder. Passes a token for notification (NB 'token' refers to the User's tokens{} property, not the datetime used to identify users submissions)
*
*  @param {String} userDir The directory to look into
*/
com.rehabstudio.utils.User.prototype.userImagesToArray = function(userDir)
{
	this.fs.folderContent(userDir,this.tokens.userimageslist);
};

/**
*  Uses the FS folderContent functionality to retrieve the complete list of users directories in the File System. No token is required
*
*  @param {Bool} isCron A flag to let the FS Service know this call is used by the cron process from the model
*/
com.rehabstudio.utils.User.prototype.readUsersDirectories = function(isCron)
{
  if(isCron){
    this.fs.folderContent('/',this.tokens.usersfolderslistcron);
  }
  else this.usersDirectories();
}

/**
*  Looks for an unprocessed set of images and sends it for save to the File System. Uses a combination of action token and submission identifier to make sure the invocation is not wrong
*
*  @param {String} actionToken  A token to target this entry in the cue - efers to the User's tokens{} property
*  @param {String} userDirToken The submission token (user identifier)
*/
com.rehabstudio.utils.User.prototype.runLocalSaveCue = function(actionToken,userDirToken)
{
  for(var i = 0;i < this.cue.length;i++){
    if(this.cue[i][0] == actionToken && this.cue[i][1] == userDirToken){
      this.fs.saveFile(this.userpath(this.cue[i][1]),this.cue[i][2], this.cue[i][3], false, this.tokens.userimagesave)
    }
  }
}

/**
*  Setter - sets local data property where images VOs and user details are stored
*
*  @param {Object} data
*/
com.rehabstudio.utils.User.prototype.setData = function(data)
{
       this.data = data || {};
}

/**
*  Creates the user folder via the FS createFolder method
*
*  @param {String} userDirName The folder name
*/
com.rehabstudio.utils.User.prototype.createUserFolder = function(userDirName)
{
	this.fs.createFolder(userDirName,this.tokens.userfoldercreate);
}

/**
*  Calls the folderContent FS method against the root to get all users folders returned, and sets the correct token to identify this operation as a folders listing
*
*/
com.rehabstudio.utils.User.prototype.usersDirectories = function()
{
	this.fs.folderContent('/',this.tokens.usersfolderslist);
}

/*
*  Checks if the current user (via local user property) has already a folder or not
*
*/
com.rehabstudio.utils.User.prototype.userFolderExists = function()
{
	var total = this.usersFolders.length;
      for(var i = 0; i < total;i++){
          if(this.usersFolders[i] == this.user){
              return true;
          }
    }
    return false;
}

/**
*  Initialises all parameters needed for users operation for a given user
*
*  @param {String} strUserDir The user directory name, made up of the user email and a datetimer identifier - parameters passed from the Model when calling the save functionality
*/
com.rehabstudio.utils.User.prototype.initUser = function(strUserDir) {
    this.user = strUserDir;
    if(!this.userFolderExists()){
        Logger.debug('User.initUser:: user folder does not exist, creating');
        this.createUserFolder(strUserDir);
        this.usersFolders.push(strUserDir);
        this.usersDirectories();//=> update list of users dirs after save
    } else {
        // @about - Legacy - unlikely, but for some reason user folder exists
        Logger.debug('User.initUser:: user folder does exist, stop trying to create');
        this.runLocalCue(this.tokens.userfoldercreate,strUserDir);
    }
}

/**
*  Assembles the path for the current user directory 
*
*  @param {String} strUserDir The user directory name, made up of the user email and a datetimer identifier - parameters passed from the Model when calling the save functionality
*/
com.rehabstudio.utils.User.prototype.userpath = function(userDir) 
{
      return '/' + userDir;
}