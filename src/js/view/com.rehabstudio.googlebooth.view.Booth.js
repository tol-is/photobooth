 /**
 * @constructor
 * @param {Object} holder The holder dom element
 */
com.rehabstudio.googlebooth.view.Booth = function(holder)
{

	var self = this;
	this.__holder  = holder;
	this._streamholder = this.__holder.find("#streamholder");
	this._video = this.__holder.find("#stream")[0];
	this._flash = this.__holder.parent().find("#flash");
	this._takePhotoBtn = this.__holder.find("#take-photo-btn");
	this._submitImagesBtn = this.__holder.find("#submit-images-btn");
	this._countdown = this.__holder.find("#countdown");
	this._photoCounter = this.__holder.find("#photo-counter");


	this.takingPhoto = false;


	this.boothTween = null;
	this.boothPosition = {alpha: 0};


	this.takePhotoBtnTween = null;
	this.takePhotoBtnPosition = {y: -130};
	this.submitPhotoBtnTween = null;
	this.submitPhotoBtnPosition = {y: -122};


	this.counterTween1 = null;
	this.counterTween1Position = {y: 70};
	this.counterTween2 = null;
	this.counterTween2Position = {y: 70};
	this.counterTween3 = null;
	this.counterTween3Position = {y: 70};

	this.counterValueTween = null;
	this.counterValuePosition = {y: 0};




	//water mark image
	this._watermark= null;


	//click sound
	this.clickSound = null;
	
	this.allowTakePhoto = false;
	this.allowSubmitPhoto = false;

	this._totalImages = 1;
	this._maxPhotos = 3;

	/**
	* Dispatched on Booth Events along with a message
	* @type {com.rehabstudio.Event}
	*/
	this.boothEvent = new com.rehabstudio.Event(this);


	/*
	* Flash View
	*/
	this.flash = new com.rehabstudio.googlebooth.ui.Flash(this._flash);
	
	this._takePhotoBtn.bind("mousedown", function()
	{
		if(!self.allowTakePhoto) return;
		self.initiateSnap();
	});

	this._submitImagesBtn.bind("mousedown", function()
	{
		//self.removeMouseListeners();
		if(!self.allowSubmitPhoto) return;
		self.boothEvent.notify({'message':'submit_images_click'});
	});


	/*
	* Countdown View
	*/
	this.countdownView = new com.rehabstudio.googlebooth.ui.Countdown(this._countdown);
	this.countdownView.countDownEvent.attach( function(sender, response)
	{
		self.takePhoto();
	});

	this.hide();
}


/**
 * Initialize Booth Stream
 */
com.rehabstudio.googlebooth.view.Booth.prototype.init = function(watermark, maxPhotos)
{
		var me = this;

		this._watermark = watermark;
		this._maxPhotos = maxPhotos;


		var output = this._video;

		this.clickSound = document.createElement('audio');
		this.clickSound.setAttribute('src', 'sounds/click.wav');
		this.clickSound.load();
		this.clickSound.addEventListener("load", function()
		{
		}, true);

		output.autoplay = true;


		window.URL = window.URL || window.webkitURL;
		navigator.getUserMedia  = navigator.getUserMedia || navigator.webkitGetUserMedia ||
		                          navigator.mozGetUserMedia || navigator.msGetUserMedia;


		if (navigator.getUserMedia)
		{
			
			navigator.getUserMedia({audio: false, video: true}, function(stream)
			{
		    	output.src = window.URL.createObjectURL(stream);
 				me.boothEvent.notify({"message": "camera_ready"});

		  	}, function()
		  	{
				me.boothEvent.notify({"message": "camera_error"});
		  	});

		} else 
		{
			me.boothEvent.notify({"message": "camera_error"});
			//output.src = ; fallback?
		}

}



/**
 * Initiate Photo Snap
 */
com.rehabstudio.googlebooth.view.Booth.prototype.initiateSnap = function()
{

		this.boothEvent.notify({"message": "init_countdown"});

		this.takingPhoto = true;

		this.allowTakePhoto = false;
		this.allowSubmitPhoto = false;
		
		this.countdownView.play();
}





/**
 * Snap Picture
 */
com.rehabstudio.googlebooth.view.Booth.prototype.takePhoto = function()
{
		
		var me = this;

		setTimeout(function()
		{
			me.clickSound.play();
			me.flash.fire();
		}, 75);


		var lowResCanvas = document.createElement("canvas");
		var lowResContext = lowResCanvas.getContext("2d");
		lowResCanvas.width = 801;
		lowResCanvas.height = 450;
		lowResContext.drawImage(this._video, 0,0, 801, 450);


		var hiResCanvas = document.createElement("canvas");
		var hiResContext = hiResCanvas.getContext("2d");
		hiResCanvas.width = 1280;
		hiResCanvas.height = 720;
		hiResContext.drawImage(this._video, 0,0, 1280, 720);
		hiResContext.drawImage(this._watermark, 1260 - this._watermark.width, 700-this._watermark.height, this._watermark.width, this._watermark.height);


		this.boothEvent.notify({"message": "photo_captured", "lowResData":lowResCanvas.toDataURL("image/jpeg"), "hiResData":hiResCanvas.toDataURL("image/jpeg")});

		setTimeout(function()
		{
			me.takingPhoto = false;
		}, 2900);

}




/**
 * Image added - upon response from the model
 */
com.rehabstudio.googlebooth.view.Booth.prototype.imagesUpdated = function(imagesLength)
{
	var self = this;

	var del = 2800;

	if(this._totalImages > imagesLength)
	{
		del = 0;
	}

	this._totalImages = imagesLength;

	this.updatePhotoCounter(del);

	this.updateBoothButtons();

}




/**
 * Update Photo Counter
 */
com.rehabstudio.googlebooth.view.Booth.prototype.updatePhotoCounter = function(delay)
{

	var self = this;
	var obj = this._photoCounter.find(".nums");

	if(this.counterValueTween) this.counterValueTween.stop();
	this.counterValueTween = new TWEEN.Tween(self.counterValuePosition)
	.to({y: 0-(((3-self._maxPhotos) + self._totalImages+1)*25)}, 600)
	.delay(delay)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		obj.css("marginTop",self.counterValuePosition.y+"px");
	}).start();

}



/**
 * Update Booth Buttons
 */
com.rehabstudio.googlebooth.view.Booth.prototype.updateBoothButtons = function()
{

	var self = this;

	if(this._totalImages === this._maxPhotos)
	{
		this.disableTakePhoto();
		this._photoCounter.addClass("disabled");
	}
	else
	{
		this.allowTakePhoto = true;
		self.enableTakePhoto();
		this._photoCounter.removeClass("disabled");
	}

	if(this._totalImages === 0)
	{
		this.disableSubmitPhoto();
	}
	else
	{
		setTimeout(function()
		{
			if(self.takingPhoto === false){
				this.allowSubmitPhoto = true;
				self.enableSubmitPhoto();
			}
				
		}, 3000);
	}

}


/**
 * Show section
 */
com.rehabstudio.googlebooth.view.Booth.prototype.show = function()
{
	this.boothEvent.notify({"message": "show-booth"});
	this.__holder.addClass("active");
	this.__holder.css("pointer-events", "all");
	this.updateBoothButtons();

	var self = this;

	if(this.boothTween) this.boothTween.stop();
	this.boothTween = new TWEEN.Tween(self.boothPosition)
	.to({alpha: 100}, 1500)
	.delay(1000)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		self._streamholder.css("opacity",(self.boothPosition.alpha/100));
	})
	.onStart(function()
	{
		self.showHidePhotoCounter(true, 600);
		self.showHideTakePhoto(true, 700);
		self.showHideSubmit(true, 800);
	})
	.start();

}


/**
 * Hide section
 */
com.rehabstudio.googlebooth.view.Booth.prototype.hide = function()
{
	var self = this;

	this.boothEvent.notify({"message": "hide-booth"});
	this.__holder.removeClass("active");

	this.__holder.css("pointer-events", "none");

	if(this.boothTween) this.boothTween.stop();
	this.boothTween = new TWEEN.Tween(self.boothPosition)
	.to({alpha: 0}, 1000)
	.delay(1000)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		self._streamholder.css("opacity",(self.boothPosition.alpha/100));
	})
	.start();

	this.showHidePhotoCounter(false, 0);
	this.showHideTakePhoto(false, 0);
	this.showHideSubmit(false, 0);
}


/**
 * Show or Hide Photo Counter
 */
com.rehabstudio.googlebooth.view.Booth.prototype.showHidePhotoCounter = function(show, delay)
{

	if(delay === undefined || delay === null) delay = 0;
	if(show === undefined || show === null) show = false;


	var marg = 0;
	var ease = TWEEN.Easing.Back.Out;
	var time = 600;

	if(show === false)
	{
		marg = 70;
		ease = TWEEN.Easing.Exponential.In;
		time = 800;
	}

	var self = this;

	var obj1 = this._photoCounter.find(".ico");
	var obj2 = this._photoCounter.find(".of");
	var obj3 = this._photoCounter.find(".totalPhotos");


	if(this.counterTween1)this.counterTween1.stop();
	this.counterTween1 = new TWEEN.Tween(self.counterTween1Position).to({y: marg}, time).delay(delay+0).easing(ease)
	.onUpdate(function()
	{
		obj1.css("marginTop",self.counterTween1Position.y+"px");
	}).start();


	if(this.counterTween2) this.counterTween2.stop();
	this.counterTween2 = new TWEEN.Tween(self.counterTween2Position).to({y: marg}, time).delay(delay+100).easing(ease)
	.onUpdate(function()
	{
		obj2.css("marginTop",self.counterTween2Position.y+"px");
	}).start();


	if(this.counterTween3) this.counterTween3.stop();
	this.counterTween3 = new TWEEN.Tween(self.counterTween3Position).to({y: marg}, time).delay(delay+200).easing(ease)
	.onUpdate(function()
	{
		obj3.css("marginTop",self.counterTween3Position.y+"px");
	}).start();

}

/**
 * Show "Take Photo" Button
 */
com.rehabstudio.googlebooth.view.Booth.prototype.showHideTakePhoto = function(show, delay)
{

	var self = this;
	
	if(delay === undefined || delay === null) delay = 0;
	if(show === undefined || show === null) show = false;


	var marg = 0;
	var ease = TWEEN.Easing.Exponential.Out;
	var time = 600;


	if(show === false){
		marg = -130;
		ease = TWEEN.Easing.Exponential.In;
		time = 800;
	}

	
	if(this.takePhotoBtnTween) this.takePhotoBtnTween.stop();
	this.takePhotoBtnTween = new TWEEN.Tween(self.takePhotoBtnPosition)
	.to({y: marg}, time)
	.delay(delay)
	.easing(ease)
	.onUpdate(function()
	{
		self._takePhotoBtn.css("marginTop",self.takePhotoBtnPosition.y+"px");
	})
	.start();

}




/**
 * Show "Take Photo" Button
 */
com.rehabstudio.googlebooth.view.Booth.prototype.showHideSubmit = function(show, delay)
{
	var self = this;
	
	if(delay === undefined || delay === null) delay = 0;
	if(show === undefined || show === null) show = false;


	
	var marg = 8;
	var ease = TWEEN.Easing.Exponential.Out;
	var time = 600;

	if(show === false)
	{
		marg = -122;
		ease = TWEEN.Easing.Exponential.In;
		time = 800;
	}


	if(this.submitPhotoBtnTween) this.submitPhotoBtnTween.stop();
	this.submitPhotoBtnTween = new TWEEN.Tween(self.submitPhotoBtnPosition)
	.to({y: marg}, time)
	.delay(delay)
	.easing(ease)
	.onUpdate(function()
	{
		self._submitImagesBtn.css("marginTop",self.submitPhotoBtnPosition.y+"px");
	})
	.start();

}

com.rehabstudio.googlebooth.view.Booth.prototype.actionDisableButtons = function()
{
	this.allowTakePhoto = false;
	this.allowSubmitPhoto = false;
}


com.rehabstudio.googlebooth.view.Booth.prototype.actionEnableButtons = function()
{
	this.allowTakePhoto = true;
	this.allowSubmitPhoto = true;
}


com.rehabstudio.googlebooth.view.Booth.prototype.disableTakePhoto = function()
{
	this.allowTakePhoto = false;
	this._takePhotoBtn.addClass("disabled");
}

com.rehabstudio.googlebooth.view.Booth.prototype.enableTakePhoto = function()
{
	this.allowTakePhoto = true;
	this._takePhotoBtn.removeClass("disabled");
}

com.rehabstudio.googlebooth.view.Booth.prototype.disableSubmitPhoto = function()
{
	this.allowSubmitPhoto = false;
	this._submitImagesBtn.addClass("disabled");
}

com.rehabstudio.googlebooth.view.Booth.prototype.enableSubmitPhoto = function()
{
	this.allowSubmitPhoto = true;
	this._submitImagesBtn.removeClass("disabled");
}

/**
 * Reset View
 */
com.rehabstudio.googlebooth.view.Booth.prototype.resetView = function()
{
	this._totalImages = 0;
	this.updatePhotoCounter(0);
	this.updateBoothButtons();
}

/**
 * Resize Function
 */
com.rehabstudio.googlebooth.view.Booth.prototype.resize = function(wid, hei)
{
	this.__holder.css("width", wid+"px");
	this.__holder.css("height", hei+"px");

	this.flash.resize(wid, hei);
}

/**
 * Translate Function
 */
com.rehabstudio.googlebooth.view.Booth.prototype.translate = function(translator)
{
	//Nothing to translate here
}