/**
 * @constructor
 * @param {Object} container The modal container dom element
 */
com.rehabstudio.googlebooth.view.Footer = function(holder)
{

	this._holder = holder;
	var self = this;
	this.tweenshow = null;
	this.position = {x: -120};
	
	this._resetBtn = this._holder.find("#reset-app-btn");
	
	this._languageButtonsHolder = this._holder.find("#languageControls");

	this._languageControls = null;

	this.footerEvent = new com.rehabstudio.Event( this );

	this.languageEvent = new com.rehabstudio.Event( this );

	this._resetBtn.bind("mousedown", function()
	{
		self.footerEvent.notify({'message':'reset_app'});
	});

}


com.rehabstudio.googlebooth.view.Footer.prototype.show = function()
{
	var self = this;
	if(this.tweenshow) this.tweenshow.stop();
	this.tweenshow = new TWEEN.Tween(self.position)
	.to({x: 20}, 1000)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		self._resetBtn.css("marginLeft",self.position.x+"px");
	}).start();

}

com.rehabstudio.googlebooth.view.Footer.prototype.hide = function()
{
	var self = this;
	if(this.tweenshow) this.tweenshow.stop();
	this.tweenshow = new TWEEN.Tween(self.position)
	.to({x: -120}, 600)
	.easing(TWEEN.Easing.Exponential.In)
	.onUpdate(function()
	{
		self._resetBtn.css("marginLeft",self.position.x+"px");
	}).start();
}


com.rehabstudio.googlebooth.view.Footer.prototype.makeLangButtons = function(languagesFromModel)
{

	var self = this;

	var languages = languagesFromModel.languages;
	var defaultLang = languagesFromModel.appDefault;

	if(languages.length <2) return;
	for(var i =0;i < languages.length;i++)
	{
		var lang = languages[i];
		var classname = "language-control";
		if(lang == defaultLang ) classname += ' selected';
		var newbutton = "<button id='langButton-" + lang + "' class='" + classname + "' data-input=\""+lang+"\" style=\"background-image:url(img/flag/"+lang+".jpg);\"></button>";
		self._languageButtonsHolder.append(newbutton);
	}


	this._languageControls = this._holder.find(".language-control");
	this._languageControls.bind("mousedown", function()
	{
		self.languageEvent.notify({'message':'lang_selected','data':this.getAttribute('data-input')});
		$j(".language-control").removeClass('selected');
		$j("#" + this.id).addClass('selected');
	});

}


com.rehabstudio.googlebooth.view.Footer.prototype.defaultLangButton = function(defaultLang)
{
	$j(".language-control").removeClass('selected');
	$j(".language-control[data-input = '" + defaultLang + "']").addClass('selected');
}

/**
 * Translate Function
 */
com.rehabstudio.googlebooth.view.Footer.prototype.translate = function(translator)
{

	translator.translate('msgReset',this._resetBtn,{'jquery':true, 'rtl':false});
}