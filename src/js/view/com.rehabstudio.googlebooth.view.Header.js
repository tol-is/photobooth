/**
 * @constructor
 * @param {Object} container The modal container dom element
 */
com.rehabstudio.googlebooth.view.Header = function(holder)
{

	this._holder = holder;
	var self = this;

}



com.rehabstudio.googlebooth.view.Header.prototype.setTopImages = function(imgObj)
{
	this._insertImage(imgObj.sponsor, "top-sponsors");
	//this._insertImage(imgObj.secondary, "top-branding");
}


com.rehabstudio.googlebooth.view.Header.prototype._insertImage = function(image, id)
{
	var imgContainer = document.createElement('div');
	this._holder.append(imgContainer);
	imgContainer.setAttribute("id", id);

	imgContainer.appendChild(image);
}
