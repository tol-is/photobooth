 /**
 * @constructor
 * @param {Object} holder The holder dom element
 */
com.rehabstudio.googlebooth.view.SubmitView = function(holder)
{
	var self = this;

	this.__holder = holder;

	this.sendImagesEvent = new com.rehabstudio.Event( this );

	this._objEmailinput = new com.rehabstudio.googlebooth.ui.EmailInput();
	this._objKeyboard = new com.rehabstudio.googlebooth.ui.Keyboard();
	
	this.blocker = this.__holder.find("#termsConditionsActiveBLocker");
	this._keyboardCont = this.__holder.find("#submit-cont");
	this._titleHeader = this.__holder.find("#submitviewTitle");
	this._termsConditionsError = this.__holder.find("#tcError");
	this._termsconditionsBox = this.__holder.find("#termsModal");
	this._termsconditionsManager = this.__holder.find(".tc-manage");
	this._termsconditionsCbx = this.__holder.find("#termsconditions-chbx");
	this._submit = this.__holder.find("#submit-btn");
	this.termsconditionsText = this.__holder.find("#termsconditionsText");
	this._keyboardSubmit = this.__holder.find('#keyboardSubmit');
	this._termsconditionsAccepted = null;

}


/*
*
*/
com.rehabstudio.googlebooth.view.SubmitView.prototype.init = function(termsConditions)
{
	var self = this;

	this.initialised = this.initialised || false;

	if(!this.initialised)
	{
		this._objEmailinput.setValidationTarget(document.getElementById('userEmailInputError'));
		this._objEmailinput.initialise('email','Your Em@il here',document.getElementById('emailinputDelegate'));
		//this._objEmailinput.setDisplayDelegate(document.getElementById('emailinputDelegate'));

		this._objKeyboard.initialise('keyboard');
		this._objKeyboard.attach_bind(this._objEmailinput.userEmailInput_KeyboardEventRespond,this._objEmailinput);
		this._objKeyboard.attach_bind(function()
		{
			self.sendImagesEvent.notify({'message':'void'});
		},this);

		this._objKeyboard.attach_bind(this.send,this);
		
		this._termsConditionsErrorVisible = false;
		this.blocker.mousedown(function()
		{
			self.manageTermsconditionsBox();
		});

		
		this._termsconditionsManager.mousedown(function()
		{
				self.manageTermsconditionsBox();
		});

		
		this._termsconditionsCbx.mousedown(function()
		{
			self.setTermsconditions();
		});


		this._submit.mousedown(function()
		{
			self._objKeyboard.typekey('_send');
		});

		this.termsconditionsText.html(termsConditions.termsconditions);
		this.sendImagesEvent.notify({"message": "submit_view_initialised"});
		this.setTermsconditions(true);

		this.tweens = {
			'keyboard_show':null,
			'title_show':null
		};

		this.position = {y:110};
		this.opacity={opacity:0};

		this.initialised = true;
	}
}


com.rehabstudio.googlebooth.view.SubmitView.prototype.show = function()
{
	
	var self = this;

	this.sendImagesEvent.notify({"message": "submit_view_showed"});
	this.__holder.addClass("active");
	
	this._objEmailinput.initialise('email');

	///show sequence start
	self.__holder.css("pointer-events", "all");

		if(self.tweens['keyboard_show']) self.tweens['keyboard_show'].stop();
		self.tweens['keyboard_show'] = new TWEEN.Tween(self.position)
		.to({y: 50}, 1200)
		.easing(TWEEN.Easing.Exponential.Out)
		.onUpdate(function()
		{
			self._keyboardCont.css("top",self.position.y+"%");
		})
		.delay(1200)
		.start();


		if(self.tweens['title_show']) self.tweens['title_show'].stop();
		self.tweens['title_show'] = new TWEEN.Tween(self.opacity)
		.to({opacity: 100}, 500)
		.easing(TWEEN.Easing.Exponential.Out)
		.onUpdate(function()
		{
			self._titleHeader.css("opacity",self.opacity.opacity / 10);
		})
		.delay(1700)
		.start();

	this.sendImagesEvent.notify({"message": "submit_view_showed"});
	this.__holder.addClass("active");

}


com.rehabstudio.googlebooth.view.SubmitView.prototype.hide = function()
{
	var self = this;

	this.sendImagesEvent.notify({"message": "submit_view_exit"});
	this.__holder.removeClass("active");

	this.__holder.css("pointer-events", "none");

		if(self.tweens['keyboard_show']) self.tweens['keyboard_show'].stop();
		self.tweens['keyboard_show'] = new TWEEN.Tween(self.position)
		.to({y: 110}, 1000)
		.easing(TWEEN.Easing.Exponential.In)
		.onUpdate(function()
		{
			self._keyboardCont.css("top",self.position.y+"%");
		}).start();

		if(self.tweens['title_show']) self.tweens['title_show'].stop();
		self.tweens['title_show'] = new TWEEN.Tween(self.opacity)
		.to({opacity: 0}, 500)
		.easing(TWEEN.Easing.Exponential.In)
		.onUpdate(function()
		{
			self._titleHeader.css("opacity",self.opacity.opacity / 10);
		}).start();

	//TODO
	this._termsconditionsBox.removeClass('active');
	this._termsconditionsBox.fadeOut('fast');
	
}



/**
 * Resize Function
 */
com.rehabstudio.googlebooth.view.SubmitView.prototype.send = function(sender,args)
{
	if(args.message === "submit_button_click")
	{
		if(!this._termsconditionsAccepted)
		{
			this.termconditionsError('show');
		}

		else
		{
			if(this._objEmailinput.runValidation())
			{
				this.sendImagesEvent.notify({"message": "user_submit_success","data":this._objEmailinput.getValue()});
				this.hide();
			}
			else
			{

			}
		}
	}
}

com.rehabstudio.googlebooth.view.SubmitView.prototype.setTermsconditions = function(boolVal)
{
	
	if(boolVal === undefined)
	{
		this._termsconditionsAccepted = this._termsconditionsAccepted?false:true;
	}
	else
	{
		this._termsconditionsAccepted = boolVal;
	}
		
	if(this._termsconditionsAccepted)
	{
		this._termsconditionsCbx.addClass('active');

		this._submit.removeClass('disabled');
		this._submit.css('pointer-events','all');
		this._keyboardSubmit.removeClass('disabled');
		this._keyboardSubmit.css('pointer-events','all');

		if(this._termsConditionsErrorVisible)
		{
			this.termconditionsError('hide');
		}
	}
	else
	{
		this._termsconditionsCbx.removeClass('active');
		this._submit.addClass('disabled');
		this._submit.css('pointer-events','none');
		this._keyboardSubmit.addClass('disabled');
		this._keyboardSubmit.css('pointer-events','none');
	}
}

com.rehabstudio.googlebooth.view.SubmitView.prototype.readTermsconditions = function()
{
	return this._termsconditionsAccepted;
}

com.rehabstudio.googlebooth.view.SubmitView.prototype.manageTermsconditionsBox = function()
{
	if(this._termsconditionsBox.hasClass('active'))
	{
		this._termsconditionsBox.removeClass('active');
		this._termsconditionsBox.fadeOut('fast');
		this.blocker.css("pointer-events", "none");
		this.blocker.removeClass('active');
	}
	else
	{
		this._termsconditionsBox.addClass('active');
		this._termsconditionsBox.fadeIn('fast');
		this.blocker.css("pointer-events", "all");
		this.blocker.addClass('active');
	}
}

com.rehabstudio.googlebooth.view.SubmitView.prototype.termconditionsError = function(boolVal)
{
	switch(boolVal)
	{
		case 'show':
			this._termsConditionsError.addClass('active');
			this._termsConditionsErrorVisible = true;
		break;

		case 'hide':
			this._termsConditionsError.removeClass('active');
			this._termsConditionsErrorVisible = false;
		break;
	}
}


com.rehabstudio.googlebooth.view.SubmitView.prototype.resetView = function()
{
	this._objEmailinput.initialise('email');
}




/**
 * Resize Function
 */
com.rehabstudio.googlebooth.view.SubmitView.prototype.resize = function(wid, hei)
{
	this.__holder.css("width", wid+"px");
	this.__holder.css("height", hei+"px");

	this.blocker.css("width", wid+"px");
	this.blocker.css("height", hei+"px");
}

/**
 * Translate Function
 */
com.rehabstudio.googlebooth.view.SubmitView.prototype.translate= function(translator)
{
	var self = this;
	translator.translate('msgSubmit',this._submit,{'jquery':'true'});
	translator.translate('termsconditions',this.termsconditionsText,{'jquery':'true'});
    translator.translate('msgTermsConditionsContent',document.getElementById('TCContainer'),{'jquery':false,'formatter':'formatTCManage'});

    this._objEmailinput.welcomeString = translator.translate('msgEmail',false,{'returnVal':true});
    this._objEmailinput.initialise(
		'email',
		this._objEmailinput.welcomeString,
		false
	);

	translator.translate('msgSendTitle',document.getElementById('submitviewTitle'),{'jquery':false});
	translator.translate('msgEmailInputError',document.getElementById('userEmailInputError'),{'jquery':false});

	// @about The translation would have removed the mousedown attached to the T&C link so let's re attach it
	this.__holder.find('#termsandconditions-modal').mousedown(function(){
		self.manageTermsconditionsBox();
	});
}