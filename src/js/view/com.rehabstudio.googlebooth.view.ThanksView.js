 /**
 * @constructor
 * @param {Object} holder The holder dom element
 */
com.rehabstudio.googlebooth.view.ThanksView = function(holder)
{

	var self = this;

	this._holder = holder;

	this.thanksEvent = new com.rehabstudio.Event( this );
	this.tweens = {
		'ui_show':null
	};
	this.opacity={opacity:0};
	this._holder.mousedown(function(){
		self.thanksEvent.notify({"message": "thanks_click_reset_please"});
	});
}

com.rehabstudio.googlebooth.view.ThanksView.prototype.show = function()
{
	var self = this;

	this._holder.addClass("active");
	this._holder.css("pointer-events", "all");

	if(this.tweens['ui_show'])self.tweens['ui_show'].stop();
	self.tweens['ui_show'] = new TWEEN.Tween(self.opacity)
	.to({opacity: 100}, 2000)
	.delay(2000)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		self._holder.css("opacity",self.opacity.opacity / 10);
	})
	.delay(1500)
	.start();
}


com.rehabstudio.googlebooth.view.ThanksView.prototype.hide = function()
{
	var self = this;

	this._holder.removeClass("active");
	this._holder.css("pointer-events", "none");

	if(this.tweens['ui_show'])self.tweens['ui_show'].stop();
	self.tweens['ui_show'] = new TWEEN.Tween(self.opacity)
	.to({opacity: 0}, 800)
	.easing(TWEEN.Easing.Exponential.In)
	.onUpdate(function()
	{
		self._holder.css("opacity",self.opacity.opacity / 10);
	})
	.start();
}

/**
 * Resize Function
 */
com.rehabstudio.googlebooth.view.ThanksView.prototype.resize = function(wid, hei)
{

	this._holder.css("width", wid+"px");
	this._holder.css("height", hei+"px");
	
}

com.rehabstudio.googlebooth.view.ThanksView.prototype.translate = function(translator)
{
	translator.translate('msgThanksMainTitle',document.getElementById('thanksMainTitle'),{'jquery':false});
	translator.translate('msgThanksSubTitle',document.getElementById('thanksSubTitle'),{'jquery':false});
	translator.translate('msgThanksParagraph',document.getElementById('thanksParagraph'),{'jquery':false});
}

