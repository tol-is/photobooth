 /**
 * @constructor
 * @param {Object} holder The holder dom element
 */
com.rehabstudio.googlebooth.view.UserImagesView = function(holder)
{

	var self = this;
	this.__holder = holder;

	this._images = new Array(4);

	this.userImagesEvent = new com.rehabstudio.Event( this );
	this.colPosition = [
		{"x": -280, "y": 60 },
		{"x": 770, "y": 60 },
		{"x": -280, "y": 230 },
		{"x": 770, "y": 230 }
	];


	this.tween = null;
	this.position = {alpha:0, y: -260};

	this._selectedPicToRemove =  null;

	
	this.bubble = this.__holder.find("#killbubble");

	this.bubbleYesBtn = this.bubble.find("#yes");
	this.bubbleYesBtn.mousedown(function(){
		self.removeSelectedImage();
	});

	this.bubbleNoBtn = this.bubble.find("#no");
	this.bubbleNoBtn.mousedown(function(){
		self.cancelRemove();
	});

	this.blocker = this.__holder.find("#mouseblocker");
	this.blocker.mousedown(function(){
		self.cancelRemove();
	});

	this.hide();
}



com.rehabstudio.googlebooth.view.UserImagesView.prototype.show = function()
{
	
	this.__holder.addClass("active");
	this.__holder.css("opacity", 1);
}


com.rehabstudio.googlebooth.view.UserImagesView.prototype.hide = function()
{
	this.__holder.removeClass("active");

}



com.rehabstudio.googlebooth.view.UserImagesView.prototype.resetView = function(imageData)
{

	var self = this;

	for(var i=0; i<this._images.length; i++){

		if(this._images[i]!=null || this._images[i]!=undefined)
		{
			
			self._images[i].kill();
			self._images[i] = null;
			
		}
	}

	this.cancelRemove();

}



com.rehabstudio.googlebooth.view.UserImagesView.prototype.onPicEvent = function(sender, response )
{
	var self = this;

	switch(true)
	{

		case response.message === "select_image_to_remove":
				this.imageSelectedToRemove(response.pic);
			break;
		default:
			Logger.debug('Uncaught notification from view.submitView.sendImagesEvent :: ' + response.message);
	}

}



com.rehabstudio.googlebooth.view.UserImagesView.prototype.imageSelectedToRemove = function(newPicToRemove)
{
	
	this._selectedPicToRemove = newPicToRemove;

	this.showBubble();
	
}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.removeSelectedImage = function()
{
	
	this.hideBubble();
	this.userImagesEvent.notify({"message": "remove_image", "imageVo":this._selectedPicToRemove._vo});
	
}


com.rehabstudio.googlebooth.view.UserImagesView.prototype.cancelRemove = function()
{

	this.hideBubble();
	this._selectedPicToRemove = null;
	
}


com.rehabstudio.googlebooth.view.UserImagesView.prototype.showBubble = function()
{

	var self = this;

	this.blocker.css("pointer-events", "all");
	this.bubble.css("pointer-events", "all");

	var offLeft = this._selectedPicToRemove._imageHolder.offset().left;
	var offTop = this._selectedPicToRemove._imageHolder.offset().top;
	this.bubble.css("left", (offLeft+40)+"px");


	this.position.alpha=0;
	this.position.y=offTop-70;

	if(this.tween) this.tween.stop();

	this.tween = new TWEEN.Tween(self.position)
	.to({alpha: 100, y: offTop-60}, 1400)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		self.bubble.css("opacity",(self.position.alpha/100));
		self.bubble.css("top",(self.position.y+"px"));
	})
	.start();

}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.hideBubble = function()
{

	var self = this;
	this.bubble.css("pointer-events", "none");
	self.blocker.css("pointer-events", "none");

	var offTop = this.bubble.offset().top;

	
	if(this.tween) this.tween.stop();
	this.tween = new TWEEN.Tween(self.position)
	.to({alpha: 0, y: offTop-20}, 1400)
	.easing(TWEEN.Easing.Exponential.Out)
	.onUpdate(function()
	{
		self.bubble.css("opacity",(self.position.alpha/100));
		self.bubble.css("top",(self.position.y+"px"));
	})
	.start();

}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.removeImage = function(imageToRemoveId)
{
	var self = this;
	var index = 0;
	for(var i=0; i<this._images.length; i++)
	{

			if(this._images[i]!=null || this._images[i]!=undefined)
			{
				var pic = self._images[i];

				if(pic.imageId == imageToRemoveId)
				{
					index = i;
					self._images[index] = null;
					pic.kill();
					pic = null;
					return;
				}
			}
	}
	//this.setToColumns();
}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.pushImage = function(imageVo)
{

	var self = this;

	var index = this.findFirstPosition();

	var newImage = new com.rehabstudio.googlebooth.ui.Pic(this.__holder, imageVo);

	this._images[index] = newImage;

	newImage.picEvent.attach( function(sender, response) {
		self.onPicEvent(sender, response );
	});

	this.arrangeNewImage(index);

}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.findFirstPosition = function()
{
	var index = 0;
	for(var i=0; i<this._images.length; i++)
	{
			if(this._images[i]==null || this._images[i]==undefined)
			{
				index = i;
				return index;
			}
	}
}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.arrangeNewImage = function(index)
{
	var self = this;

	var newX = -345 + this.colPosition[index].x;
	var newY = -230 + this.colPosition[index].y;
	

	setTimeout(function()
	{
		self._images[index].moveToSide(newX,newY, 0, true);
	}, 2000);
	
}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.setToColumns = function()
{
	
	var self = this;
	
	for(var i=0; i<this._images.length; i++)
	{
		if(this._images[i]!=null || this._images[i]!=undefined)
		{
			
			var newX = -345 + self.colPosition[i].x;
			var newY = -230 + self.colPosition[i].y;
  
			self._images[i].moveToPosition(newX,newY,i*200, true);
		}
	}

}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.setToRow = function()
{
	var tmpImages = [];
	for(var i=0; i<this._images.length; i++)
	{
		if(this._images[i]!=null || this._images[i]!=undefined)
		{
			tmpImages.push(this._images[i]);
		}
	}

	var index = 0;
	var topY = -50;
	var firstX = 0-(tmpImages.length * 180)/2;

	tmpImages.forEach(function(){

		var newX = 0 + firstX;
		var newY = -130 + topY;
  
		tmpImages[index].moveToPosition(newX,newY,1600+(index*200), false);

		firstX +=188;
		index++;
	});

	tmpImages=null;
}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.imagesSubmitted = function()
{
	var tmpImages = [];
	
	for(var i=0; i<this._images.length; i++)
	{
		if(this._images[i]!=null || this._images[i]!=undefined)
		{
			tmpImages.push(this._images[i]);
		}
	}
	var cnt = 0;
	for(var i=tmpImages.length-1; i>=0; i--)	
	{
		tmpImages[i].moveOutside(400+(cnt*200));
		cnt++;

	}
	tmpImages=null;
}

com.rehabstudio.googlebooth.view.UserImagesView.prototype.hideAllImages = function()
{

}

/**
 * Resize Function
 */
com.rehabstudio.googlebooth.view.UserImagesView.prototype.resize = function(wid, hei)
{

	this.__holder.css("width", wid+"px");
	this.__holder.css("height", hei+"px");

	this.blocker.css("width", wid+"px");
	this.blocker.css("height", hei+"px");
	
}

/**
 * Translate Function
 */
com.rehabstudio.googlebooth.view.UserImagesView.prototype.translate = function(translator)
{
	translator.translate('msgNo',this.bubbleNoBtn,{'jquery':true});
	translator.translate('msgYes',this.bubbleYesBtn,{'jquery':true});
	translator.translate('msgDelete',$j('#user-images-view h3.delete'),{'jquery':true});
}