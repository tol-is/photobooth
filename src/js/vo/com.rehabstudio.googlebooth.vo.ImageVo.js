
/**
 * ImageVo component of the Google Photobooth MVC
 * @param {*} imageData The imageData for this vo
 * @param {*} id The image id
 * @constructor
 */
com.rehabstudio.googlebooth.vo.ImageVo = function( lowResData,imageData, id )
{
	this.id = id
	this.lowResData = lowResData;

	this.imageData = imageData;
}


com.rehabstudio.googlebooth.vo.ImageVo.prototype.getLowResData = function()
{
	var toReturn = this.lowResData;
	this.lowResData = null;
	return toReturn;
}



